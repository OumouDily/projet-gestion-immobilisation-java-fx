/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.BlacklistDAO;

import DAOFactory.DAOFactory;
import Models.Blacklist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class BlacklistDAOImpl implements BlacklistDAO {

    private final DAOFactory daoFactory;

    public BlacklistDAOImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    Connection connexion;
    Statement statement;

    ResultSet resultat;
    PreparedStatement preparedStatement;

    @Override
    public boolean Ajouter(Blacklist blacklist) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("INSERT INTO blacklist(idB,idMembre) VALUES(?,?)");
            preparedStatement.setInt(1, blacklist.getIdB());
            preparedStatement.setInt(2, blacklist.getIdMembre());
//            preparedStatement.setString(3, membre.getMdp());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public Blacklist rechercherParId(Integer idB) {
        Blacklist blacklist = new Blacklist();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM blacklist where idMembre  = ?");
            preparedStatement.setInt(1, idB);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                blacklist.setIdB(resultat.getInt(1));
                blacklist.setIdMembre(resultat.getInt(2));
            } else {
                return null;
            }
            return blacklist;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

}
