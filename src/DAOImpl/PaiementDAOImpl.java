/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.PaiementDAO;
import DAOFactory.DAOFactory;
import Models.Membre;
import Models.Paiement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class PaiementDAOImpl implements PaiementDAO {

    private final DAOFactory daoFactory;

    public PaiementDAOImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    Connection connexion;
    Statement statement;

    ResultSet resultat;
    PreparedStatement preparedStatement;

    @Override
    public boolean Ajouter(Paiement paiement) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("INSERT INTO paiement(dateP,reference,montantPaye,typePaiement,cdeFacture) VALUES(?,?,?,?,?)");
            preparedStatement.setDate(1, paiement.getDateP());
            preparedStatement.setString(2, paiement.getReference());
            preparedStatement.setDouble(3, paiement.getMontantPaye());
            preparedStatement.setString(4, paiement.getTypePaiement());
            preparedStatement.setString(5, paiement.getCdeFacture());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

//    @Override
//    public Paiement comparer(String code) {
//        Paiement paiement = new Paiement();
//        connexion = null;
//        try {
//            connexion = daoFactory.getConnection();
//            preparedStatement = connexion.prepareStatement("SELECT dateP,reference,montantPaye,typePaiement,cdeFacture from paiement p,facture f WHERE p.montantPaye=f.montantTotal ");
//            preparedStatement.setString(1, code);
//            resultat = preparedStatement.executeQuery();
//            if (resultat.next()) {
//                paiement.setId(resultat.getInt(1));
//                paiement.setNom(resultat.getString(2));
//                paiement.setEmail(resultat.getString(3));
//                paiement.setMdp(resultat.getString(4));
//            } else {
//                return null;
//            }
//            return paiement;
//        } catch (SQLException e) {
//        } finally {
//            if (connexion != null) {
//                try {
//                    preparedStatement.close();
//                    connexion.close();
//                } catch (SQLException ex) {
//                }
//            }
//        }
//        return null;
//    }
    @Override
    public Paiement rechercherParCode(String code) {
        Paiement paiement = new Paiement();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM paiement WHERE cdeFacture = ?");
            preparedStatement.setString(1, code);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                paiement.setIdPaiement(resultat.getInt(1));
                paiement.setDateP(resultat.getDate(2));
                paiement.setReference(resultat.getString(3));
                paiement.setMontantPaye(resultat.getDouble(4));
                paiement.setTypePaiement(resultat.getString(5));
                paiement.setCdeFacture(resultat.getString(6));
            } else {
                return null;
            }
            return paiement;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

}
