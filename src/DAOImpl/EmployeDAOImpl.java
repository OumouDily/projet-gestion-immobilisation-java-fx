/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.EmployeDAO;
import DAOFactory.DAOFactory;
import Models.Employe;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class EmployeDAOImpl implements EmployeDAO {

    private final DAOFactory daoFactory;

    public EmployeDAOImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    Connection connexion;
    Statement statement;

    ResultSet resultat;
    PreparedStatement preparedStatement;

    @Override
    public boolean Ajouter(Employe employe) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("INSERT INTO employe(nomAgent,nomServ,nomAgence) VALUES(?,?,?)");
//            preparedStatement.setString(1, employe.getCdeAgent());
            preparedStatement.setString(1, employe.getNomAgent());
//            preparedStatement.setString(3, employe.getPrenomAgent());
            preparedStatement.setString(2, employe.getNomServ());
            preparedStatement.setString(3, employe.getNomAgence());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public List<Employe> listerEmployes() {
        List<Employe> employes = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM employe");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Employe employe = new Employe();
                employe.setId(resultat.getInt(1));
                employe.setCdeAgent(resultat.getInt(2));
                employe.setNomAgent(resultat.getString(3));
                employe.setPrenomAgent(resultat.getString(4));
                employe.setNomServ(resultat.getString(5));
                employe.setNomAgence(resultat.getString(6));
                employes.add(employe);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return employes;
    }

    @Override
    public Employe rechercher(String nomAgent) {
        Employe emp = new Employe();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM employe WHERE nomAgent = ?");
            preparedStatement.setString(1, nomAgent);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                emp.setId(resultat.getInt("id"));
                emp.setNomAgent(resultat.getString("nomAgent"));
                emp.setNomServ(resultat.getString("nomServ"));
                emp.setNomAgence(resultat.getString("nomAgence"));
            } else {
                return null;
            }
            return emp;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

    @Override
    public boolean modifierEmploye(Employe employe) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("UPDATE employe SET cdeAgent = ?, nomAgent = ?, prenomAgent=?, nomServ=?, nomAgence=? WHERE id = ?");
            preparedStatement.setInt(1, employe.getCdeAgent());
            preparedStatement.setString(2, employe.getNomAgent());
            preparedStatement.setString(3, employe.getPrenomAgent());
            preparedStatement.setString(4, employe.getNomServ());
            preparedStatement.setString(5, employe.getNomAgence());
            preparedStatement.setInt(6, employe.getId());

            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public Employe rechercherParId(Integer idAgent) {
        Employe emp = new Employe();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM employe WHERE id = ?");
            preparedStatement.setInt(1, idAgent);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                emp.setId(resultat.getInt(1));
                emp.setCdeAgent(resultat.getInt(2));
                emp.setNomAgent(resultat.getString(3));
                emp.setPrenomAgent(resultat.getString(4));
                emp.setNomServ(resultat.getString(5));
                emp.setNomAgence(resultat.getString(6));
            } else {
                return null;
            }
            return emp;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

}
