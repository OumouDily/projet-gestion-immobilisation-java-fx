/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.EquipementDAO;
import DAOFactory.DAOFactory;
import Models.Equipement;
import Models.Membre;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class EquipementDAOImpl implements EquipementDAO {

    private final DAOFactory daoFactory;

    public EquipementDAOImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    Connection connexion;
    Statement statement;

    ResultSet resultat;
    PreparedStatement preparedStatement;

    @Override
    public boolean Ajouter(Equipement equipement) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("INSERT INTO equipement(cde_equip,nom_equip,type_immo,R_C,compte,descrcpte,montant,date,etat,taux_amort,validation,nom_agent,idReferenceFacture) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");
            preparedStatement.setString(1, equipement.getCde_equip());
            preparedStatement.setString(2, equipement.getNom_equip());
            preparedStatement.setString(3, equipement.getType_immo());
            preparedStatement.setString(4, equipement.getR_C());
            preparedStatement.setInt(5, equipement.getCompte());
            preparedStatement.setString(6, equipement.getDescrcpte());
            preparedStatement.setDouble(7, equipement.getMontant());

            preparedStatement.setDate(8, equipement.getDate());
            preparedStatement.setString(9, equipement.getEtat());
            preparedStatement.setDouble(10, equipement.getTaux_amort());
            preparedStatement.setString(11, equipement.getValidation());
            preparedStatement.setString(12, equipement.getNom_agent());
            preparedStatement.setInt(13, equipement.getIdReferenceFacture());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public List<Equipement> listerEquipements() {
        List<Equipement> equipements = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select type_immo, etat, date, nom_equip, montant, nom_agent,validation from equipement eq, employe ag where eq.nom_agent = ag.nomAgent ");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Equipement equipement = new Equipement();

//                equipement.setCde_equip(resultat.getString("cde_equip"));
                equipement.setType_immo(resultat.getString("type_immo"));
                equipement.setEtat(resultat.getString("etat"));
                equipement.setDate(resultat.getDate("date"));
                equipement.setNom_equip(resultat.getString("nom_equip"));
                equipement.setMontant(resultat.getDouble("montant"));
                equipement.setNom_agent(resultat.getString("nom_agent"));
                equipement.setValidation(resultat.getString("validation"));

                equipements.add(equipement);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return equipements;
    }

    @Override
    public List<Equipement> listerEquipementsParService(String nomS) {
        List<Equipement> equipements = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select type_immo, etat, date, nom_equip, montant, nom_agent,validation from equipement eq, employe em, service se where eq.nom_agent=em.nomAgent and em.nomServ = se.nomServ and em.nomServ = ? and eq.validation='Validé'");
            preparedStatement.setString(1, nomS);
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Equipement equipement = new Equipement();
//                equipement.setId(resultat.getInt("id"));
//                equipement.setCde_equip(resultat.getString(1));
                equipement.setType_immo(resultat.getString(1));
                equipement.setEtat(resultat.getString(2));
                equipement.setDate(resultat.getDate(3));
                equipement.setNom_equip(resultat.getString(4));
                equipement.setMontant(resultat.getDouble(5));
                equipement.setNom_agent(resultat.getString(6));

                equipements.add(equipement);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return equipements;
    }

    @Override
    public List<Equipement> listerEquipementsParNomAgent(String nomA) {
        List<Equipement> equipements = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select cde_equip, type_immo, etat, date, nom_equip, montant, nom_agent,validation from equipement eq, employe ag where eq.nom_agent = ag.nomAgent and ag.nomAgent=? and eq.validation='Validé'");
            preparedStatement.setString(1, nomA);
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Equipement equipement = new Equipement();
//                equipement.setId(resultat.getInt("id"));
                equipement.setCde_equip(resultat.getString(1));
                equipement.setType_immo(resultat.getString(2));
                equipement.setEtat(resultat.getString(3));
                equipement.setDate(resultat.getDate(4));
                equipement.setNom_equip(resultat.getString(5));
                equipement.setMontant(resultat.getDouble(6));
                equipement.setNom_agent(resultat.getString(7));

                equipements.add(equipement);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return equipements;
    }

    @Override
    public List<Equipement> listerEquipementsParEtat(String etats) {
        List<Equipement> equipements = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select cde_equip, type_immo, etat, date, nom_equip, montant, nom_agent,validation from equipement eq, employe ag where eq.etat = ? and eq.nom_agent=ag.nomAgent and eq.validation='Validé'");
            preparedStatement.setString(1, etats);
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Equipement equipement = new Equipement();
//                equipement.setId(resultat.getInt("id"));
                equipement.setCde_equip(resultat.getString(1));
                equipement.setType_immo(resultat.getString(2));
                equipement.setEtat(resultat.getString(3));
                equipement.setDate(resultat.getDate(4));
                equipement.setNom_equip(resultat.getString(5));
                equipement.setMontant(resultat.getDouble(6));
                equipement.setNom_agent(resultat.getString(7));

                equipements.add(equipement);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return equipements;
    }

    @Override
    public List<Equipement> listerEquipementsParAgence(String nomAg) {
        List<Equipement> equipements = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select cde_equip, type_immo, etat, date, nom_equip, montant, nom_agent,validation from equipement eq, employe ag, agence a where eq.nom_agent=ag.nomAgent AND ag.nomAgence=a.nomAgence  AND a.nomAgence=? and eq.validation='Validé'");
            preparedStatement.setString(1, nomAg);
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Equipement equipement = new Equipement();
//                equipement.setId(resultat.getInt("id"));
                equipement.setCde_equip(resultat.getString(1));
                equipement.setType_immo(resultat.getString(2));
                equipement.setEtat(resultat.getString(3));
                equipement.setDate(resultat.getDate(4));
                equipement.setNom_equip(resultat.getString(5));
                equipement.setMontant(resultat.getDouble(6));
                equipement.setNom_agent(resultat.getString(7));

                equipements.add(equipement);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return equipements;
    }

    @Override
    public List<Equipement> listerEquipementsParDep(String nomDep) {
        List<Equipement> equipements = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select type_immo, etat, date, nom_equip, montant, nom_agent,validation from equipement eq, employe ag,service s,departement dep where eq.nom_agent = ag.nomAgent and ag.nomServ=s.nomServ and s.cdeDep=dep.codeDep and dep.nomDep = ? and eq.validation='Validé'");
            preparedStatement.setString(1, nomDep);
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Equipement equipement = new Equipement();
//                equipement.setId(resultat.getInt("id"));
//                equipement.setCde_equip(resultat.getString(1));
                equipement.setType_immo(resultat.getString(1));
                equipement.setEtat(resultat.getString(2));
                equipement.setDate(resultat.getDate(3));
                equipement.setNom_equip(resultat.getString(4));
                equipement.setMontant(resultat.getDouble(5));
                equipement.setNom_agent(resultat.getString(6));

                equipements.add(equipement);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return equipements;
    }

    @Override
    public List<Equipement> listerEquipementsVente() {
        List<Equipement> equipements = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select type_immo, montant, nom_equip,nom_agent from equipement eq, employe emp where eq.nom_agent=emp.nomAgent");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Equipement equipement = new Equipement();

//                equipement.setCde_equip(resultat.getString("cde_equip"));
                equipement.setType_immo(resultat.getString("type_immo"));
                equipement.setMontant(resultat.getDouble("montant"));
                equipement.setNom_equip(resultat.getString("nom_equip"));
                equipement.setNom_agent(resultat.getString("nom_agent"));

                equipements.add(equipement);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return equipements;
    }

    @Override
    public boolean modifierEquipement(Equipement equipement) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("UPDATE equipement SET cde_equip = ?, nom_equip = ?, type_immo=?, R_C=?, compte=?,descrcpte=?,montant=?,date=?,etat=?,taux_amort=?,validation=?,nom_agent=? WHERE id = ?");
            preparedStatement.setString(1, equipement.getCde_equip());
            preparedStatement.setString(2, equipement.getNom_equip());
            preparedStatement.setString(3, equipement.getType_immo());
            preparedStatement.setString(4, equipement.getR_C());
            preparedStatement.setInt(5, equipement.getCompte());
            preparedStatement.setString(6, equipement.getDescrcpte());
            preparedStatement.setDouble(7, equipement.getMontant());
            preparedStatement.setDate(8, equipement.getDate());
            preparedStatement.setString(9, equipement.getEtat());
            preparedStatement.setDouble(10, equipement.getTaux_amort());
            preparedStatement.setString(11, equipement.getValidation());
            preparedStatement.setString(12, equipement.getNom_agent());
            preparedStatement.setInt(13, equipement.getId());

            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public Equipement rechercherParNom(String nom) {
        Equipement equipement = new Equipement();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM equipement WHERE nom_equip = ?");
            preparedStatement.setString(1, nom);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                equipement.setId(resultat.getInt(1));
                equipement.setCde_equip(resultat.getString(2));
                equipement.setNom_equip(resultat.getString(3));
                equipement.setType_immo(resultat.getString(4));
                equipement.setR_C(resultat.getString(5));
                equipement.setCompte(resultat.getInt(6));
                equipement.setDescrcpte(resultat.getString(7));
                equipement.setMontant(resultat.getDouble(8));
                equipement.setDate(resultat.getDate(9));
                equipement.setEtat(resultat.getString(10));
                equipement.setTaux_amort(resultat.getDouble(11));
                equipement.setValidation(resultat.getString(12));
                equipement.setNom_agent(resultat.getString(13));
            } else {
                return null;
            }
            return equipement;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

    @Override
    public List<Equipement> listerEquipementsAffect() {
        List<Equipement> equipements = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select type_immo,nom_equip,nom_agent,date from equipement eq, employe ag where eq.nom_agent = ag.nomAgent");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Equipement equipement = new Equipement();

//                equipement.setCde_equip(resultat.getString("cde_equip"));
                equipement.setType_immo(resultat.getString("type_immo"));
//            
                equipement.setNom_equip(resultat.getString("nom_equip"));
                equipement.setNom_agent(resultat.getString("nom_agent"));
                equipement.setDate(resultat.getDate("date"));

                equipements.add(equipement);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return equipements;
    }

    @Override
    public List<Equipement> listerEquipementAmort() {
        List<Equipement> equipements = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select nom_equip,type_immo,nom_agent,montant,taux_amort,R_C,(montant*taux_amort)/12 AS amortcmmM ,(montant*taux_amort)/12 AS amort_M , (montant*taux_amort)/12 AS v_n_c from equipement eq,employe emp where eq.nom_agent=emp.nomAgent");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Equipement equipement = new Equipement();

//                equipement.setCde_equip(resultat.getString("cde_equip"));
                equipement.setNom_equip(resultat.getString("nom_equip"));
                equipement.setType_immo(resultat.getString("type_immo"));

                equipement.setNom_agent(resultat.getString("nom_agent"));
                equipement.setMontant(resultat.getDouble("montant"));
                equipement.setTaux_amort(resultat.getDouble("taux_amort"));
                equipement.setR_C(resultat.getString("R_C"));

                equipements.add(equipement);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return equipements;
    }

    @Override
    public List<Equipement> listerEquipementsAmortis() {
       List<Equipement> equipements = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select type_immo, etat, date, nom_equip, montant, nom_agent,validation from equipement eq, employe ag where eq.etat = 'perdu' and eq.nom_agent=ag.nomAgent and eq.validation='Validé'");
//            preparedStatement.setString(1, etat);
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Equipement equipement = new Equipement();
//                equipement.setId(resultat.getInt("id"));
//                equipement.setCde_equip(resultat.getString(1));
                equipement.setType_immo(resultat.getString(1));
                equipement.setEtat(resultat.getString(2));
                equipement.setDate(resultat.getDate(3));
                equipement.setNom_equip(resultat.getString(4));
                equipement.setMontant(resultat.getDouble(5));
                equipement.setNom_agent(resultat.getString(6));

                equipements.add(equipement);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return equipements;
    }

}
