/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.FournisseurDAO;
import DAOFactory.DAOFactory;
import Models.Facture;
import Models.Fournisseur;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class FournisseurDAOImpl implements FournisseurDAO {

    private final DAOFactory daoFactory;

    public FournisseurDAOImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    Connection connexion;
    Statement statement;

    ResultSet resultat;
    PreparedStatement preparedStatement;

    @Override
    public boolean Ajouter(Fournisseur fournisseur) {
         connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("INSERT INTO fournisseur(nomFourn,adresse,numFiscal) VALUES(?,?,?)");
            preparedStatement.setString(1, fournisseur.getNomFourn());

            preparedStatement.setString(2, fournisseur.getAdresse());
            preparedStatement.setString(3, fournisseur.getNumFiscal());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public List<Fournisseur> listerFournisseurs() {
        List<Fournisseur> fournisseurs = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select nomFourn,adresse,numFiscal from fournisseur");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Fournisseur fournisseur = new Fournisseur();
//                facture.setNumFacture(resultat.getString("numFacture"));
                fournisseur.setNomFourn(resultat.getString("nomFourn"));
              
                fournisseur.setAdresse(resultat.getString("adresse"));
                fournisseur.setNumFiscal(resultat.getString("numFiscal"));

                fournisseurs.add(fournisseur);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return fournisseurs;
    }

    @Override
    public Fournisseur rechercherParNomFournisseur(String nomFournisseur) {
         Fournisseur fournisseur = new Fournisseur();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select id, nomFourn, adresse, numFiscal from fournisseur where nomFourn = ?");
            preparedStatement.setString(1, nomFournisseur);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {

                fournisseur.setId(resultat.getInt(1));
                fournisseur.setNomFourn(resultat.getString(2));
              
                fournisseur.setAdresse(resultat.getString(3));
                fournisseur.setNumFiscal(resultat.getString(4));
            } else {
                return null;
            }
            return fournisseur;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

    @Override
    public boolean modifierFournisseur(Fournisseur fournisseur) {
         connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("UPDATE fournisseur SET nomFourn = ?, adresse=?, numFiscal=? WHERE id = ?");
            preparedStatement.setString(1, fournisseur.getNomFourn());

            preparedStatement.setString(2, fournisseur.getAdresse());
            preparedStatement.setString(3, fournisseur.getNumFiscal());
         
            preparedStatement.setInt(4, fournisseur.getId());

            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public Fournisseur rechercherParId(Integer idF) {
     Fournisseur fournisseur = new Fournisseur();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM fournisseur WHERE id = ?");
            preparedStatement.setInt(1, idF);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                fournisseur.setId(resultat.getInt(1));
                fournisseur.setNomFourn(resultat.getString(2));
               
                fournisseur.setAdresse(resultat.getString(3));
                fournisseur.setNumFiscal(resultat.getString(4));
                
            } else {
                return null;
            }
            return fournisseur;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

}
