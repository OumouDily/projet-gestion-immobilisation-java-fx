/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.DepartementDAO;
import DAOFactory.DAOFactory;
import Models.Departement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class DepartementDAOImpl implements DepartementDAO{
    private final DAOFactory daoFactory;
    public DepartementDAOImpl(DAOFactory daoFactory)
    {
        this.daoFactory=daoFactory;
    }
    Connection connexion;
    Statement statement;

    ResultSet resultat;
    PreparedStatement preparedStatement;
    

    @Override
    public boolean Ajouter(Departement departement) {
    connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("INSERT INTO departement(codeDep,nomDep) VALUES(?,?)");
            preparedStatement.setString(1, departement.getCodeDep());
            preparedStatement.setString(2, departement.getNomDep());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public List<Departement> listerDepartements() {
        List<Departement> departements = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM departement");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Departement departement = new Departement();
                departement.setId(resultat.getInt("id"));
                departement.setCodeDep(resultat.getString("codeDep"));
                departement.setNomDep(resultat.getString("nomDep"));
//                departement.setMdp(resultat.getString("mdp"));

                departements.add(departement);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return departements;
    }
}
