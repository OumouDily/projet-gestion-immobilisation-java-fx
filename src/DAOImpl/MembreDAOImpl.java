/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.MembreDAO;
import DAOFactory.DAOFactory;
import Models.Membre;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class MembreDAOImpl implements MembreDAO {

    private final DAOFactory daoFactory;

    public MembreDAOImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    Connection connexion;
    Statement statement;

    ResultSet resultat;
    PreparedStatement preparedStatement;

    @Override
    public boolean Ajouter(Membre membre) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("INSERT INTO membre(nom,email,mdp,statut,rang) VALUES(?,?,?,?,?)");
            preparedStatement.setString(1, membre.getNom());
            preparedStatement.setString(2, membre.getEmail());
            preparedStatement.setString(3, membre.getMdp());
            preparedStatement.setString(4, membre.getStatut());
            preparedStatement.setInt(5, membre.getRang());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public Membre rechercher(String nom, String mdp) {
        Membre membre = new Membre();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM membre WHERE nom = ? AND mdp = ?");
            preparedStatement.setString(1, nom);
            preparedStatement.setString(2, mdp);

            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                membre.setId(resultat.getInt("id"));
                membre.setNom(resultat.getString("nom"));
                membre.setEmail(resultat.getString("email"));
                membre.setMdp(resultat.getString("mdp"));
                membre.setStatut(resultat.getString("statut"));

            } else {
                return null;
            }
            return membre;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

    @Override
    public List<Membre> listerMembres() {
        List<Membre> membres = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM membre");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Membre membre = new Membre();
                membre.setId(resultat.getInt("id"));
                membre.setNom(resultat.getString("nom"));
                membre.setEmail(resultat.getString("email"));
                membre.setMdp(resultat.getString("mdp"));
                membre.setRang(resultat.getInt("rang"));

                membres.add(membre);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return membres;
    }

    @Override
    public boolean supprimerMembre(Membre membre) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("DELETE FROM membre WHERE id = ?");
            preparedStatement.setInt(1, membre.getId());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public boolean modifierMembre(Membre membre) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("UPDATE membre SET nom = ?, email = ?, mdp=?, statut=?,rang=? WHERE id = ?");
            preparedStatement.setString(1, membre.getNom());
            preparedStatement.setString(2, membre.getEmail());
            preparedStatement.setString(3, membre.getMdp());
            preparedStatement.setString(4, membre.getStatut());
            preparedStatement.setInt(5, membre.getRang());
            preparedStatement.setInt(6, membre.getId());

            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public Membre rechercherParId(Integer id) {

        Membre membre = new Membre();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM membre WHERE id = ?");
            preparedStatement.setInt(1, id);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                membre.setId(resultat.getInt(1));
                membre.setNom(resultat.getString(2));
                membre.setEmail(resultat.getString(3));
                membre.setMdp(resultat.getString(4));
                membre.setStatut(resultat.getString(5));
                membre.setRang(resultat.getInt(6));

            } else {
                return null;
            }
            return membre;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

}
