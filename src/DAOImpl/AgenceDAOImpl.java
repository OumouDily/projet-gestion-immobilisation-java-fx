/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.AgenceDAO;
import DAOFactory.DAOFactory;
import Models.Agence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class AgenceDAOImpl implements AgenceDAO{

     private final DAOFactory daoFactory;

    public AgenceDAOImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    Connection connexion;
    Statement statement;

    ResultSet resultat;
    PreparedStatement preparedStatement;
    @Override
    public List<Agence> listerAgences() {
        List<Agence> agences = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM agence");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Agence agence = new Agence();
                agence.setCdeAgence(resultat.getInt("cdeAgence"));
                agence.setNomAgence(resultat.getString("nomAgence"));
               

                agences.add(agence);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return agences;
    }

  
}
