/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.ServiceDAO;
import DAOFactory.DAOFactory;
import Models.Employe;

import Models.Service;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class ServiceDAOImpl implements ServiceDAO {

    private final DAOFactory daoFactory;

    public ServiceDAOImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    Connection connexion;
    Statement statement;

    ResultSet resultat;
    PreparedStatement preparedStatement;

    @Override
    public boolean Ajouter(Service service) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("INSERT INTO service(cdeServ,nomServ,cdeDep) VALUES(?,?,?)");
            preparedStatement.setString(1, service.getCdeServ());
            preparedStatement.setString(2, service.getNomServ());
            preparedStatement.setString(3, service.getCdeDep());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

   
    @Override
    public List<Service> listerServices() {
        List<Service> services = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM service");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Service service = new Service();
                service.setId(resultat.getInt("id"));
                service.setCdeServ(resultat.getString("cdeServ"));
                service.setNomServ(resultat.getString("nomServ"));
                service.setCdeDep(resultat.getString("cdeDep"));
//                departement.setMdp(resultat.getString("mdp"));

                services.add(service);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return services;
    }

    @Override
    public Service rechercher(String nomServ) {
         Service serv = new Service();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM service WHERE nomServ = ?");
            preparedStatement.setString(1, nomServ);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                serv.setId(resultat.getInt("id"));
                serv.setCdeServ(resultat.getString("cdeServ"));
                serv.setNomServ(resultat.getString("nomServ"));
                serv.setCdeDep(resultat.getString("cdeDep"));
            } else {
                return null;
            }
            return serv;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

    @Override
    public Service rechercherParId(Integer id) {
          Service serv = new Service();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM service WHERE id = ?");
            preparedStatement.setInt(1, id);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                serv.setId(resultat.getInt(1));
                serv.setCdeServ(resultat.getString(2));
                serv.setNomServ(resultat.getString(3));
                serv.setCdeDep(resultat.getString(4));
               
            } else {
                return null;
            }
            return serv;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

    @Override
    public boolean modifierService(Service service) {
         connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("UPDATE service SET cdeServ = ?, nomServ = ?, cdeDep=? WHERE id = ?");
            preparedStatement.setString(1, service.getCdeServ());
            preparedStatement.setString(2, service.getNomServ());
            preparedStatement.setString(3, service.getCdeDep());
            preparedStatement.setInt(4, service.getId());

            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

}
