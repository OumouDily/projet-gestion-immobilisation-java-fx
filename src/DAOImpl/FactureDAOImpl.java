/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.FactureDAO;
import DAOFactory.DAOFactory;
import Models.Employe;
import Models.Facture;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class FactureDAOImpl implements FactureDAO {

    private final DAOFactory daoFactory;

    public FactureDAOImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    Connection connexion;
    Statement statement;

    ResultSet resultat;
    PreparedStatement preparedStatement;

    @Override
    public boolean Ajouter(Facture facture) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("INSERT INTO facture(reference,numFacture,date, nomFournisseur, montantTotal, taxe,statutFacture) VALUES(?,?,?,?,?,?,?)");
            preparedStatement.setInt(1, facture.getReference());
            preparedStatement.setString(2, facture.getNumFacture());
            preparedStatement.setDate(3, facture.getDate());
            preparedStatement.setString(4, facture.getNomFournisseur());
            preparedStatement.setDouble(5, facture.getMontantTotal());
            preparedStatement.setString(6, facture.getTaxe());
            preparedStatement.setString(7, facture.getStatutFacture());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public List<Facture> listerFacturesAPayer() {
        List<Facture> factures = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select numFacture, nomFournisseur,date,montantTotal,taxe,statutFacture from facture where statutFacture='Non payée'");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Facture facture = new Facture();
                facture.setNumFacture(resultat.getString("numFacture"));
                facture.setNomFournisseur(resultat.getString("nomFournisseur"));
                facture.setDate(resultat.getDate("date"));
                facture.setMontantTotal(resultat.getDouble("montantTotal"));
                facture.setTaxe(resultat.getString("taxe"));
                facture.setStatutFacture(resultat.getString("statutFacture"));

                factures.add(facture);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return factures;
    }

    @Override
    public Facture rechercherParNomFournisseur(String nomFournisseur) {
        Facture facture = new Facture();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select id, nomFournisseur, date, montantTotal, taxe from facture where nomFournisseur = ?");
            preparedStatement.setString(1, nomFournisseur);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {

                facture.setId(resultat.getInt(1));
//                facture.setNumFacture(resultat.getString(1));
                facture.setNomFournisseur(resultat.getString(2));
                facture.setDate(resultat.getDate(3));
                facture.setMontantTotal(resultat.getDouble(4));
                facture.setTaxe(resultat.getString(5));
            } else {
                return null;
            }
            return facture;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

    @Override
    public List<Facture> listerFacturesPayees() {
        List<Facture> factures = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select nomFournisseur,date,montantTotal,taxe,statutFacture from facture where statutFacture='Payée'");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Facture facture = new Facture();
//                facture.setNumFacture(resultat.getString("numFacture"));
                facture.setNomFournisseur(resultat.getString("nomFournisseur"));
                facture.setDate(resultat.getDate("date"));
                facture.setMontantTotal(resultat.getDouble("montantTotal"));
                facture.setTaxe(resultat.getString("taxe"));
                facture.setStatutFacture(resultat.getString("statutFacture"));

                factures.add(facture);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return factures;
    }

    @Override
    public List<Facture> listerFacturesAttente() {
        List<Facture> factures = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select nomFournisseur,date,montantTotal,taxe,statutFacture from facture where statutFacture='Non payée' ");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Facture facture = new Facture();
//                facture.setNumFacture(resultat.getString("numFacture"));
                facture.setNomFournisseur(resultat.getString("nomFournisseur"));
                facture.setDate(resultat.getDate("date"));
                facture.setMontantTotal(resultat.getDouble("montantTotal"));
                facture.setTaxe(resultat.getString("taxe"));
                facture.setStatutFacture(resultat.getString("statutFacture"));

                factures.add(facture);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return factures;
    }

    @Override
    public List<Facture> listerFacturess() {
        List<Facture> factures = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select numFacture,nomFournisseur,date,montantTotal,taxe, (montantTotal - montantPaye) AS reste from facture f, paiement p where f.numFacture = p.cdeFacture ");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Facture facture = new Facture();
                facture.setNumFacture(resultat.getString("numFacture"));
                facture.setNomFournisseur(resultat.getString("nomFournisseur"));
                facture.setDate(resultat.getDate("date"));
                facture.setMontantTotal(resultat.getDouble("montantTotal"));
                facture.setTaxe(resultat.getString("taxe"));
//                facture.setReste(resultat.getDouble("reste"));

                factures.add(facture);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return factures;
    }

    @Override
    public boolean modifierFacture(Facture facture) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("UPDATE facture SET reference=?,numFacture = ?, date = ?, nomFournisseur=?, montantTotal=?, taxe=?,statutFacture WHERE id = ?");
            preparedStatement.setInt(1, facture.getReference());
            preparedStatement.setString(2, facture.getNumFacture());
            preparedStatement.setDate(3, facture.getDate());
            preparedStatement.setString(4, facture.getNomFournisseur());
            preparedStatement.setDouble(5, facture.getMontantTotal());
            preparedStatement.setString(6, facture.getTaxe());
            preparedStatement.setString(7, facture.getStatutFacture());
            preparedStatement.setInt(8, facture.getId());

            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public Facture rechercherParId(Integer idF) {
        Facture facture = new Facture();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM facture WHERE id = ?");
            preparedStatement.setInt(1, idF);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                facture.setId(resultat.getInt(1));
                facture.setNumFacture(resultat.getString(2));
                facture.setDate(resultat.getDate(3));
                facture.setNomFournisseur(resultat.getString(4));
                facture.setMontantTotal(resultat.getDouble(5));
                facture.setTaxe(resultat.getString(6));
                facture.setStatutFacture(resultat.getString(7));
            } else {
                return null;
            }
            return facture;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

    @Override
    public List<Facture> listerFactures() {
        List<Facture> factures = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select *from facture ");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Facture facture = new Facture();
                facture.setReference(resultat.getInt("reference"));
                facture.setNumFacture(resultat.getString("numFacture"));
                facture.setNomFournisseur(resultat.getString("nomFournisseur"));
                facture.setDate(resultat.getDate("date"));
                facture.setMontantTotal(resultat.getDouble("montantTotal"));
                facture.setTaxe(resultat.getString("taxe"));
//                facture.setReste(resultat.getDouble("reste"));

                factures.add(facture);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return factures;
    }

}
