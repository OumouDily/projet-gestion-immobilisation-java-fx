/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.VenteDAO;
import DAOFactory.DAOFactory;
import Models.Equipement;
import Models.Membre;
import Models.Vente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class VenteDAOImpl implements VenteDAO {

    private final DAOFactory daoFactory;

    public VenteDAOImpl(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }
    Connection connexion;
    Statement statement;

    ResultSet resultat;
    PreparedStatement preparedStatement;

    @Override
    public boolean Ajouter(Vente vente) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("INSERT INTO vente(codeVente,typeImmo,nomImmo,prixVente,nomAcheteur,etat) VALUES(?,?,?,?,?,?)");
            preparedStatement.setString(1, vente.getCodeVente());
            preparedStatement.setString(2, vente.getTypeImmo());
            preparedStatement.setString(3, vente.getNomImmo());
            preparedStatement.setDouble(4, vente.getPrixVente());
            preparedStatement.setString(5, vente.getNomAcheteur());
            preparedStatement.setString(6, vente.getEtat());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public List<Vente> listerVenteNonConf() {
        List<Vente> ventes = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select typeImmo, nomImmo, prixVente,nomAcheteur from vente");
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Vente vente = new Vente();

//                vente.setCodeVente(resultat.getString("codeVente"));
                vente.setTypeImmo(resultat.getString(1));
                vente.setNomImmo(resultat.getString(2));
                vente.setPrixVente(resultat.getDouble(3));
                vente.setNomAcheteur(resultat.getString(4));
//                vente.setEtat(resultat.getString("etat"));

                ventes.add(vente);
            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return ventes;
    }

    @Override
    public boolean modifierVente(Vente vente) {
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("UPDATE vente SET codeVente = ?, typeImmo = ?, nomImmo=?, prixVente=?,nomAcheteur=?,etat=? WHERE id = ?");
            preparedStatement.setString(1, vente.getCodeVente());
            preparedStatement.setString(2, vente.getTypeImmo());
            preparedStatement.setString(3, vente.getNomImmo());
            preparedStatement.setDouble(4, vente.getPrixVente());
            preparedStatement.setString(5, vente.getNomAcheteur());
            preparedStatement.setString(6, vente.getEtat());
            preparedStatement.setInt(7, vente.getId());

            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

//    @Override
//    public Vente rechercherParId(Integer id) {
//        Vente vente = new Vente();
//        connexion = null;
//        try {
//            connexion = daoFactory.getConnection();
//            preparedStatement = connexion.prepareStatement("SELECT * FROM vente WHERE id = ?");
//            preparedStatement.setInt(1, id);
//            resultat = preparedStatement.executeQuery();
//            if (resultat.next()) {
//                vente.setId(resultat.getInt(1));
//                vente.setCodeVente(resultat.getString(2));
//                vente.setTypeImmo(resultat.getString(3));
//                vente.setNomImmo(resultat.getString(4));
//                vente.setPrixVente(resultat.getDouble(5));
//                vente.setNomAcheteur(resultat.getString(6));
//                vente.setEtat(resultat.getString(7));
//            } else {
//                return null;
//            }
//            return vente;
//        } catch (SQLException e) {
//        } finally {
//            if (connexion != null) {
//                try {
//                    preparedStatement.close();
//                    connexion.close();
//                } catch (SQLException ex) {
//                }
//            }
//        }
//        return null;
//    }
    @Override
    public Vente rechercherParNom(String nom) {
        Vente vente = new Vente();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM vente WHERE nomImmo = ?");
            preparedStatement.setString(1, nom);
            resultat = preparedStatement.executeQuery();
            if (resultat.next()) {
                vente.setId(resultat.getInt(1));
                vente.setCodeVente(resultat.getString(2));
                vente.setTypeImmo(resultat.getString(3));
                vente.setNomImmo(resultat.getString(4));
                vente.setPrixVente(resultat.getDouble(5));
                vente.setNomAcheteur(resultat.getString(6));
                vente.setEtat(resultat.getString(7));
            } else {
                return null;
            }
            return vente;
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    preparedStatement.close();
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return null;
    }

    @Override
    public List<Vente> listerEquipVendu(String etat) {
        List<Vente> ventes = new ArrayList<>();
        connexion = null;
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("select typeImmo, nomImmo, prixVente,nomAcheteur,etat from vente where etat=?");
            preparedStatement.setString(1, etat);
            resultat = preparedStatement.executeQuery();
            while (resultat.next()) {
                Vente vente = new Vente();

//                vente.setCodeVente(resultat.getString(1));
                vente.setTypeImmo(resultat.getString(1));
                vente.setNomImmo(resultat.getString(2));
                vente.setPrixVente(resultat.getDouble(3));
                vente.setNomAcheteur(resultat.getString(4));
                vente.setEtat(resultat.getString(5));

                ventes.add(vente);

            }
        } catch (SQLException e) {
        } finally {
            if (connexion != null) {
                try {
                    connexion.close();
                } catch (SQLException ex) {
                }
            }
        }
        return ventes;
    }

}
