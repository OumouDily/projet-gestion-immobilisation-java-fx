/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class Vente {
    private Integer id;
    private String codeVente;
    private String typeImmo;
    private String nomImmo;
    private Double prixVente;
    private String nomAcheteur;
    private String etat;

    public Vente() {
    }

    public Vente(Integer id, String codeVente, String typeImmo, String nomImmo, Double prixVente, String nomAcheteur, String etat) {
        this.id = id;
        this.codeVente = codeVente;
        this.typeImmo = typeImmo;
        this.nomImmo = nomImmo;
        this.prixVente = prixVente;
        this.nomAcheteur = nomAcheteur;
        this.etat = etat;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodeVente() {
        return codeVente;
    }

    public void setCodeVente(String codeVente) {
        this.codeVente = codeVente;
    }

    public String getTypeImmo() {
        return typeImmo;
    }

    public void setTypeImmo(String typeImmo) {
        this.typeImmo = typeImmo;
    }

    public String getNomImmo() {
        return nomImmo;
    }

    public void setNomImmo(String nomImmo) {
        this.nomImmo = nomImmo;
    }

    public Double getPrixVente() {
        return prixVente;
    }

    public void setPrixVente(Double prixVente) {
        this.prixVente = prixVente;
    }

    public String getNomAcheteur() {
        return nomAcheteur;
    }

    public void setNomAcheteur(String nomAcheteur) {
        this.nomAcheteur = nomAcheteur;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    
    
}
