/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class Employe {

    private Integer id;
    private Integer cdeAgent;
    private String nomAgent;
    private String prenomAgent;
    private String nomServ;
    private String nomAgence;

    public Employe() {
    }

    public Employe(Integer id, Integer cdeAgent, String nomAgent, String prenomAgent, String nomServ, String nomAgence) {
        this.id = id;
        this.cdeAgent = cdeAgent;
        this.nomAgent = nomAgent;
        this.prenomAgent = prenomAgent;
        this.nomServ = nomServ;
        this.nomAgence = nomAgence;
    }

   

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCdeAgent() {
        return cdeAgent;
    }

    public void setCdeAgent(Integer cdeAgent) {
        this.cdeAgent = cdeAgent;
    }

    public String getNomAgent() {
        return nomAgent;
    }

    public void setNomAgent(String nomAgent) {
        this.nomAgent = nomAgent;
    }

    public String getPrenomAgent() {
        return prenomAgent;
    }

    public void setPrenomAgent(String prenomAgent) {
        this.prenomAgent = prenomAgent;
    }

    public String getNomServ() {
        return nomServ;
    }

    public void setNomServ(String nomServ) {
        this.nomServ = nomServ;
    }

    public String getNomAgence() {
        return nomAgence;
    }

    public void setNomAgence(String nomAgence) {
        this.nomAgence = nomAgence;
    }

   

   
}
