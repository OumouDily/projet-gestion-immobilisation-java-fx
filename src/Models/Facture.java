/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Date;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class Facture {

    private Integer id;
    private Integer reference;
    private String numFacture;
    private Date date;
    private String nomFournisseur;
    private Double montantTotal;
    private String taxe;
 
    private String statutFacture;
    public Facture() {
    }

    public Facture(Integer id, Integer reference, String numFacture, Date date, String nomFournisseur, Double montantTotal, String taxe, String statutFacture) {
        this.id = id;
        this.reference = reference;
        this.numFacture = numFacture;
        this.date = date;
        this.nomFournisseur = nomFournisseur;
        this.montantTotal = montantTotal;
        this.taxe = taxe;
        this.statutFacture = statutFacture;
    }

 

  

    public Facture(Integer id, String numFacture, Date date, String nomFournisseur, Double montantTotal, String taxe) {
        this.id = id;
        this.numFacture = numFacture;
        this.date = date;
        this.nomFournisseur = nomFournisseur;
        this.montantTotal = montantTotal;
        this.taxe = taxe;
    }

   

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumFacture() {
        return numFacture;
    }

    public void setNumFacture(String numFacture) {
        this.numFacture = numFacture;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNomFournisseur() {
        return nomFournisseur;
    }

    public void setNomFournisseur(String nomFournisseur) {
        this.nomFournisseur = nomFournisseur;
    }

    public Double getMontantTotal() {
        return montantTotal;
    }

    public void setMontantTotal(Double montantTotal) {
        this.montantTotal = montantTotal;
    }

    public String getTaxe() {
        return taxe;
    }

    public void setTaxe(String taxe) {
        this.taxe = taxe;
    }

  
    public String getStatutFacture() {
        return statutFacture;
    }

    public void setStatutFacture(String statutFacture) {
        this.statutFacture = statutFacture;
    }

    public Integer getReference() {
        return reference;
    }

    public void setReference(Integer reference) {
        this.reference = reference;
    }

   

}
