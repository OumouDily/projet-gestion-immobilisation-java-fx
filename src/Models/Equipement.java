/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Date;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class Equipement {

    private Integer id;
    private String cde_equip;
    private String type_immo;
    private String etat;
    private Date date;
    private String nom_equip;
    private Double Montant;
    private String nom_agent;
    private String R_C;
    private Integer compte;
    private String descrcpte;

    private Double taux_amort;
    private String validation;
  
    private String nomServ;
    private Integer idReferenceFacture;

    public Equipement() {
    }

    public Equipement(Integer id, String cde_equip, String type_immo, String etat, Date date, String nom_equip, Double Montant, String nom_agent, String R_C, Integer compte, String descrcpte, Double taux_amort, String validation, String nomServ, Integer idReferenceFacture) {
        this.id = id;
        this.cde_equip = cde_equip;
        this.type_immo = type_immo;
        this.etat = etat;
        this.date = date;
        this.nom_equip = nom_equip;
        this.Montant = Montant;
        this.nom_agent = nom_agent;
        this.R_C = R_C;
        this.compte = compte;
        this.descrcpte = descrcpte;
        this.taux_amort = taux_amort;
        this.validation = validation;
        this.nomServ = nomServ;
        this.idReferenceFacture = idReferenceFacture;
    }

  

    public Equipement(String type_immo, String etat, Date date, String nom_equip, Double Montant, String nom_agent, Integer compte, Double taux_amort) {
        this.type_immo = type_immo;
        this.etat = etat;
        this.date = date;
        this.nom_equip = nom_equip;
        this.Montant = Montant;
        this.nom_agent = nom_agent;
        this.compte = compte;
        this.taux_amort = taux_amort;
    }

 

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCde_equip() {
        return cde_equip;
    }

    public void setCde_equip(String cde_equip) {
        this.cde_equip = cde_equip;
    }

    public String getType_immo() {
        return type_immo;
    }

    public void setType_immo(String type_immo) {
        this.type_immo = type_immo;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNom_equip() {
        return nom_equip;
    }

    public void setNom_equip(String nom_equip) {
        this.nom_equip = nom_equip;
    }

    public Double getMontant() {
        return Montant;
    }

    public void setMontant(Double Montant) {
        this.Montant = Montant;
    }

    public String getNom_agent() {
        return nom_agent;
    }

    public void setNom_agent(String nom_agent) {
        this.nom_agent = nom_agent;
    }

    public String getR_C() {
        return R_C;
    }

    public void setR_C(String R_C) {
        this.R_C = R_C;
    }

    public Integer getCompte() {
        return compte;
    }

    public void setCompte(Integer compte) {
        this.compte = compte;
    }

  

    public String getDescrcpte() {
        return descrcpte;
    }

    public void setDescrcpte(String descrcpte) {
        this.descrcpte = descrcpte;
    }

    public Double getTaux_amort() {
        return taux_amort;
    }

    public void setTaux_amort(Double taux_amort) {
        this.taux_amort = taux_amort;
    }

    public String getValidation() {
        return validation;
    }

    public void setValidation(String validation) {
        this.validation = validation;
    }

    public String getNomServ() {
        return nomServ;
    }

    public void setNomServ(String nomServ) {
        this.nomServ = nomServ;
    }

    public Integer getIdReferenceFacture() {
        return idReferenceFacture;
    }

    public void setIdReferenceFacture(Integer idReferenceFacture) {
        this.idReferenceFacture = idReferenceFacture;
    }

  

}
