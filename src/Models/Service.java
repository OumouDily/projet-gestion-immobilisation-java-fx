/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class Service {
     private Integer id;
   private String cdeServ;
   private String nomServ;
   private String cdeDep;

    public Service() {
    }

    public Service(Integer id, String cdeServ, String nomServ, String cdeDep) {
        this.id = id;
        this.cdeServ = cdeServ;
        this.nomServ = nomServ;
        this.cdeDep = cdeDep;
    }

   

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCdeServ() {
        return cdeServ;
    }

    public void setCdeServ(String cdeServ) {
        this.cdeServ = cdeServ;
    }

    public String getNomServ() {
        return nomServ;
    }

    public void setNomServ(String nomServ) {
        this.nomServ = nomServ;
    }

    public String getCdeDep() {
        return cdeDep;
    }

    public void setCdeDep(String cdeDep) {
        this.cdeDep = cdeDep;
    }

   
   
}
