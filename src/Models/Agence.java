/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class Agence {
     private Integer cdeAgence;
    private String nomAgence;

    public Agence() {
    }

    public Agence(Integer cdeAgence, String nomAgence) {
        this.cdeAgence = cdeAgence;
        this.nomAgence = nomAgence;
    }

    public Integer getCdeAgence() {
        return cdeAgence;
    }

    public void setCdeAgence(Integer cdeAgence) {
        this.cdeAgence = cdeAgence;
    }

    public String getNomAgence() {
        return nomAgence;
    }

    public void setNomAgence(String nomAgence) {
        this.nomAgence = nomAgence;
    }
    
    
    
}
