/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Date;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class Paiement {
     private Integer idPaiement;
     private Date dateP;
    private String reference;
    private Double montantPaye;
    private String typePaiement;
    private String cdeFacture;

    public Paiement() {
    }

    public Paiement(Integer idPaiement, Date dateP, String reference, Double montantPaye, String typePaiement, String cdeFacture) {
        this.idPaiement = idPaiement;
        this.dateP = dateP;
        this.reference = reference;
        this.montantPaye = montantPaye;
        this.typePaiement = typePaiement;
        this.cdeFacture = cdeFacture;
    }

    public String getCdeFacture() {
        return cdeFacture;
    }

    public void setCdeFacture(String cdeFacture) {
        this.cdeFacture = cdeFacture;
    }

   

    public Integer getIdPaiement() {
        return idPaiement;
    }

    public void setIdPaiement(Integer idPaiement) {
        this.idPaiement = idPaiement;
    }

    public Date getDateP() {
        return dateP;
    }

    public void setDateP(Date dateP) {
        this.dateP = dateP;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getMontantPaye() {
        return montantPaye;
    }

    public void setMontantPaye(Double montantPaye) {
        this.montantPaye = montantPaye;
    }

    public String getTypePaiement() {
        return typePaiement;
    }

    public void setTypePaiement(String typePaiement) {
        this.typePaiement = typePaiement;
    }
    
}
