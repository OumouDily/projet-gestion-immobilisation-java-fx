/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class Blacklist {
     private Integer idB;
      private Integer idMembre;

 

    public Blacklist() {
    }

    public Blacklist(Integer idB, Integer idMembre) {
        this.idB = idB;
        this.idMembre = idMembre;
    }

    public Integer getIdB() {
        return idB;
    }

    public void setIdB(Integer idB) {
        this.idB = idB;
    }

    public Integer getIdMembre() {
        return idMembre;
    }

    public void setIdMembre(Integer idMembre) {
        this.idMembre = idMembre;
    }

   
   
}
