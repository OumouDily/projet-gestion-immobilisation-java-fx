/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class Fournisseur {
    private Integer id;
    private String nomFourn;
   
    private String adresse;
    private String numFiscal;

    public Fournisseur() {
    }

    public Fournisseur(Integer id, String nomFourn, String adresse, String numFiscal) {
        this.id = id;
        this.nomFourn = nomFourn;
        this.adresse = adresse;
        this.numFiscal = numFiscal;
    }

   

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomFourn() {
        return nomFourn;
    }

    public void setNomFourn(String nomFourn) {
        this.nomFourn = nomFourn;
    }

   

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNumFiscal() {
        return numFiscal;
    }

    public void setNumFiscal(String numFiscal) {
        this.numFiscal = numFiscal;
    }
    
    
}
