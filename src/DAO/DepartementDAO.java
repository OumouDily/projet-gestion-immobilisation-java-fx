/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Departement;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public interface DepartementDAO {
    public boolean Ajouter(Departement departement);
    public List<Departement> listerDepartements();
}
