/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Membre;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public interface MembreDAO {

    public boolean Ajouter(Membre membre);

    public Membre rechercher(String nom, String mdp);

    public List<Membre> listerMembres();

    public boolean supprimerMembre(Membre membre);

    public boolean modifierMembre(Membre membre);
    
    public Membre rechercherParId(Integer id);
    
    
    

//    public void supprimerMembre(Membre membre);
}
