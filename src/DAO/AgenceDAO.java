/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Agence;

import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public interface AgenceDAO {
    public List<Agence> listerAgences();
}
