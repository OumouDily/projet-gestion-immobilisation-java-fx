/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Blacklist;



/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public interface BlacklistDAO {
    public boolean Ajouter(Blacklist blacklist);
    public Blacklist rechercherParId(Integer idB);
}
