/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Fournisseur;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public interface FournisseurDAO {

    public boolean Ajouter(Fournisseur fournisseur);

    public List<Fournisseur> listerFournisseurs();

    public Fournisseur rechercherParNomFournisseur(String nomFournisseur);

    public boolean modifierFournisseur(Fournisseur fournisseur);

    public Fournisseur rechercherParId(Integer idF);
}
