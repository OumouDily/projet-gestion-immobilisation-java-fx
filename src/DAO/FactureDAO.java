/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Facture;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public interface FactureDAO {

    public boolean Ajouter(Facture facture);

    public List<Facture> listerFacturesAPayer();

    public List<Facture> listerFacturess();
    
     public List<Facture> listerFactures();

    public List<Facture> listerFacturesAttente();

    public List<Facture> listerFacturesPayees();

    public Facture rechercherParNomFournisseur(String nomFournisseur);
    
    public boolean modifierFacture(Facture facture);

    public Facture rechercherParId(Integer idF);

}
