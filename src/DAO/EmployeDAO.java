/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Employe;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public interface EmployeDAO {

    public boolean Ajouter(Employe employe);

    public List<Employe> listerEmployes();

    public Employe rechercher(String nomAgent);
    
    public boolean modifierEmploye(Employe employe);

    public Employe rechercherParId(Integer idAgent);
   
}
