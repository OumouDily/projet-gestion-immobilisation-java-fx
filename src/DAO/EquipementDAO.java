/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Equipement;
import Models.Membre;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public interface EquipementDAO {

    public boolean Ajouter(Equipement equipement);

    public List<Equipement> listerEquipements();

    public List<Equipement> listerEquipementsAffect();

    public List<Equipement> listerEquipementsVente();
    
    public List<Equipement> listerEquipementAmort();

    public List<Equipement> listerEquipementsParService(String nomS);

    public List<Equipement> listerEquipementsParNomAgent(String nomA);

    public List<Equipement> listerEquipementsParEtat(String etats);

    public List<Equipement> listerEquipementsParAgence(String nomAg);

    public List<Equipement> listerEquipementsParDep(String nomDep);
    
    public List<Equipement> listerEquipementsAmortis();

    public boolean modifierEquipement(Equipement equipement);

    public Equipement rechercherParNom(String nom);
}
