/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;


import Models.Service;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public interface ServiceDAO {
    public boolean Ajouter(Service service);
    public List<Service> listerServices();
    public Service rechercher(String nomServ);
    public Service rechercherParId(Integer id);
    public boolean modifierService(Service service);
}
