/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Paiement;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public interface PaiementDAO {

    public boolean Ajouter(Paiement paiement);
    public Paiement rechercherParCode(String code);

//    public Paiement comparer(String code);
}
