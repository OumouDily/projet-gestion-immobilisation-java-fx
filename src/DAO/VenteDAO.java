/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Vente;
import java.util.List;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public interface VenteDAO {

    public boolean Ajouter(Vente vente);

    public List<Vente> listerVenteNonConf();

    public boolean modifierVente(Vente vente);

    public Vente rechercherParNom(String nom);
    
     public List<Vente> listerEquipVendu(String etat);
}
