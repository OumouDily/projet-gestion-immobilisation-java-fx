/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.MembreDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Membre;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class ModifierProfilValidateurController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    public TextField txtNom;
    @FXML
    public TextField txtEmail;
    @FXML
    public TextField txtMdp;
    @FXML
    private Button btnValider;
    @FXML
    public TextField txtIdSession;
    @FXML
    public TextField txtNomSession;
    @FXML
    public TextField txtEmailSession;
    @FXML
    public TextField txtMdpSession;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.membreDAO = daoFactory.getMembreDAO();
    }

    MembreDAO membreDAO;

    Message message;
    DAOFactory daoFactory;
    ObservableList<Membre> data = FXCollections.observableArrayList();

    @FXML
    private void retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/MenuValidateur.fxml"));
            loader.load();
            MenuValidateurController mc = loader.getController();
            mc.txtIdSession.setText(txtIdSession.getText());
            mc.txtEmailSession.setText(txtEmail.getText());
            mc.txtMdpSession.setText(txtMdp.getText());
            mc.txtNomSession.setText(txtNom.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentificationValidateur.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void modifierProfil(ActionEvent event) {
        Integer id = Integer.parseInt(txtIdSession.getText());
        String nom = txtNom.getText();
        String email = txtEmail.getText();
        String mdp = txtMdp.getText();

        if (!nom.isEmpty() && !email.isEmpty() && !mdp.isEmpty()) {
            Membre membre = membreDAO.rechercherParId(id);
            if (membre != null) {
            
                membre.setNom(nom);
                membre.setEmail(email);
                membre.setMdp(mdp);
                membre.setStatut(membre.getStatut());

                if (membreDAO.modifierMembre(membre)) {
                    message = new Message("INFORMATION", "INFORMATION", "Service", "Modifié avec succès");
                    ((Node) (event.getSource())).getScene().getWindow().hide();
                    exit();
                } else {
                    message = new Message("ERROR", "Erreur", "Service", "Echec de la modification");
                }

            }

        } else {
            message = new Message("ERROR", "Erreur", "INFORMATION", "Au moins un des champs est vide");
        }
    }

    private void exit() {
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentificationValidateur.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

}
