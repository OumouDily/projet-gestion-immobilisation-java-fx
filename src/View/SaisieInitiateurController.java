/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.AgenceDAO;
import DAO.DepartementDAO;
import DAO.EmployeDAO;
import DAO.FactureDAO;
import DAO.FournisseurDAO;
import DAO.ServiceDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Agence;
import Models.Departement;
import Models.Employe;
import Models.Facture;
import Models.Fournisseur;
import Models.Service;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class SaisieInitiateurController implements Initializable {

    @FXML
    private TextField txtCode;
    @FXML
    private TextField txtNom;
    @FXML
    private Button btnValider;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private ComboBox<String> ListCdeDep;
    @FXML
    private TextField txtCodeServ;
    @FXML
    private TextField txtNomServ;
    @FXML
    private Button btnValiderServ;
    @FXML
    private TextField txtNomFourn;
    @FXML
    private Button btnValiderFourn;
    @FXML
    private TextField txtAdresseFourn;
    @FXML
    private TextField txtNumFisFourn;
    private FournisseurDAO fournisseurDAO;
    @FXML
    private Hyperlink retour;
    @FXML
    private TextField txtNumFact;
    @FXML
    private DatePicker dateFact;
    private TextField nomFact;
    @FXML
    private TextField montantFact;
    @FXML
    private RadioButton TTC;
    @FXML
    private RadioButton HT;
    @FXML
    private Button btnValiderFact;
    @FXML
    private TextField txtNomAgent;
    @FXML
    private ComboBox<String> listNomServ;
    @FXML
    private ComboBox<String> listNomAgence;
    @FXML
    private Button btnValiderEmp;
    @FXML
    private Button btnValiderImmo;
    @FXML
    private ToggleGroup Group;
    @FXML
    private ComboBox<String> cmbCompteImmo;
    @FXML
    public TextField txtIdSession;
    @FXML
    public TextField txtNomSession;
    @FXML
    public TextField txtEmailSession;
    @FXML
    public TextField txtMdpSession;
    @FXML
    public TextField txtNom1;
    @FXML
    public TextField txtEmail;
    @FXML
    public TextField txtMdp;
    @FXML
    private ComboBox<String> cmbTypeImmo;
    @FXML
    private TextField txtTypeSession;
    @FXML
    private TextField txtCompteSession;
    @FXML
    private TextField txtRef;
    @FXML
    private ComboBox<String> cmbNom;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.departementDAO = daoFactory.getDepartementDAO();
        this.fournisseurDAO = daoFactory.getFournisseurDAO();
        this.factureDAO = daoFactory.getFactureDAO();
        this.serviceDAO = daoFactory.getServiceDAO();
        this.employeDAO = daoFactory.getEmployeDAO();
        this.agenceDAO = daoFactory.getAgenceDAO();
        remplirCombo(ListCdeDep);
        remplirComboEmp(listNomServ);
        remplirComboNom(cmbNom);
        remplirComboAgence(listNomAgence);
//        ObservableList<Integer> cmpImmo = FXCollections.observableArrayList(24100, 24200, 24300, 24400, 24500, 24600);
        ObservableList<String> typeImmo = FXCollections.observableArrayList("MATEREIEL ET OUTILLAGE INDUSTRIEL ET COMMERCIAL", "MATERIEL ET OUTILLAGE AGRICOLE", "MATERIEL D'EMBALLAGE RECUPERABLE ET IDENTIFIABLE", "MATERIEL ET MOBILIER", "MATERIEL DE TRANSPORT", "ACTIFS BIOLOGIQUES");
//        cmbCompteImmo.setItems(cmpImmo);
        cmbTypeImmo.setItems(typeImmo);
    }

    Message message;
    DepartementDAO departementDAO;
    FournisseurDAO fournisseurtDAO;
    FactureDAO factureDAO;
    ServiceDAO serviceDAO;
    EmployeDAO employeDAO;
    AgenceDAO agenceDAO;
    DAOFactory daoFactory;

    @FXML
    private void Recuperer(ActionEvent event) {
        String codeDep = txtCode.getText();
        String nomDep = txtNom.getText();

        if (!codeDep.isEmpty() && !nomDep.isEmpty()) {
            Departement departement = new Departement();
            departement.setCodeDep(codeDep);
            departement.setNomDep(nomDep);

            if (departementDAO.Ajouter(departement)) {
                ListCdeDep.getItems().clear();
                remplirCombo(ListCdeDep);
                txtCode.clear();
                txtNom.clear();
                message = new Message("INFORMATION", "INFORMATION", "Enregistrement", "Enregistré avec succès");

            } else {
                message = new Message("ERROR", "Erreur", "Enregistrement", "Echec de l'enregistrement");
            }

        } else {
            message = new Message("ERROR", "Erreur", "Enregistrement", "Au moins un des champs est vide");
        }
    }

    @FXML
    private void RecupererServ(ActionEvent event) {
        String cdeServ = txtCodeServ.getText();
        String nomServ = txtNomServ.getText();
        String cdeDep = ListCdeDep.getValue();

        if (!cdeServ.isEmpty() && !nomServ.isEmpty()) {
            Service service = new Service();
            service.setCdeServ(cdeServ);
            service.setNomServ(nomServ);
            service.setCdeDep(cdeDep);

            if (serviceDAO.Ajouter(service)) {
                listNomServ.getItems().clear();
                remplirComboEmp(listNomServ);
                message = new Message("INFORMATION", "INFORMATION", "Enregistrement", "Enregistré avec succès");
                txtCodeServ.clear();
                txtNomServ.clear();
            } else {
                message = new Message("ERROR", "Erreur", "Enregistrement", "Echec de l'enregistrement");
            }

        } else {
            message = new Message("ERROR", "Erreur", "Enregistrement", "Au moins un des champs est vide");
        }
    }

    @FXML
    private void AjoutEmp(ActionEvent event) {
        String nomAgent = txtNomAgent.getText();
        String nomServ = listNomServ.getValue();
        String nomAgence = listNomAgence.getValue();

        if (!nomAgent.isEmpty()) {
            Employe employe = new Employe();
            employe.setNomAgent(nomAgent);
            employe.setNomServ(nomServ);
            employe.setNomAgence(nomAgence);

            if (employeDAO.Ajouter(employe)) {
//                listNomServ.getItems().clear();
//                remplirCombo(listNomServ);
                message = new Message("INFORMATION", "INFORMATION", "Enregistrement", "Enregistré avec succès");
                txtNomAgent.clear();

            } else {
                message = new Message("ERROR", "Erreur", "Enregistrement", "Echec de l'enregistrement");
            }

        } else {
            message = new Message("ERROR", "Erreur", "Enregistrement", "Au moins un des champs est vide");
        }
    }

    @FXML
    private void RecupererFourn(ActionEvent event) {
        String nomFourn = txtNomFourn.getText();

        String adresse = txtAdresseFourn.getText();
        String numFiscal = txtNumFisFourn.getText();

        if (!nomFourn.isEmpty() && !adresse.isEmpty() && !numFiscal.isEmpty()) {
            Fournisseur fournisseur = new Fournisseur();
            fournisseur.setNomFourn(nomFourn);

            fournisseur.setAdresse(adresse);
            fournisseur.setNumFiscal(numFiscal);

            if (fournisseurDAO.Ajouter(fournisseur)) {
                cmbNom.getItems().clear();
                remplirComboNom(cmbNom);
                message = new Message("INFORMATION", "INFORMATION", "Enregistrement", "Enregistré avec succès");
                txtNomFourn.clear();

                txtAdresseFourn.clear();
                txtNumFisFourn.clear();
            } else {
                message = new Message("ERROR", "Erreur", "Enregistrement", "Echec de l'enregistrement");
            }

        } else {
            message = new Message("ERROR", "Erreur", "Enregistrement", "Au moins un des champs est vide");
        }
    }

    @FXML
    private void RecupererFact(ActionEvent event) {
        Integer reference = Integer.parseInt(txtRef.getText().trim());
        String numFacture = txtNumFact.getText();
        LocalDate ld = dateFact.getValue();
        Date sqlDate = Date.valueOf(ld);
        String nomFournisseur = cmbNom.getValue();
        Double montantTotal = Double.parseDouble(montantFact.getText().trim());
        String taxe;
        if (TTC.isSelected()) {
            taxe = "TTC";
        } else {
            taxe = "HT";
        }

        if (!numFacture.isEmpty() && !nomFournisseur.isEmpty()) {
            Facture facture = new Facture();
            facture.setReference(reference);
            facture.setNumFacture(numFacture);
            facture.setDate(sqlDate);
            facture.setNomFournisseur(nomFournisseur);
            facture.setMontantTotal(montantTotal);
            facture.setTaxe(taxe);
            facture.setStatutFacture("Non payée");
//
            if (factureDAO.Ajouter(facture)) {

                message = new Message("INFORMATION", "INFORMATION", "Enregistrement", "Enregistré avec succès");
                txtNumFact.clear();
//                nomFact.clear();
                montantFact.clear();
            } else {
                message = new Message("ERROR", "Erreur", "Enregistrement", "Echec de l'enregistrement");
            }
//
        } else {
            message = new Message("ERROR", "Erreur", "Enregistrement", "Au moins un des champs est vide");
        }
    }

    @FXML
    private void rediriger(ActionEvent event) {

        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/equipement.fxml"));
            loader.load();
            EquipementController ec = loader.getController();
            ec.txtCodeEquip.setText(cmbTypeImmo.getValue());
            ec.txtCompte.setText(String.valueOf(cmbCompteImmo.getValue()));

            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentificationInitiateur.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/MenuInitiateur.fxml"));
            loader.load();
            MenuInitiateurController mc = loader.getController();
            mc.txtIdSession.setText(txtIdSession.getText());
            mc.txtEmailSession.setText(txtEmail.getText());
            mc.txtMdpSession.setText(txtMdp.getText());
            mc.txtNomSession.setText(txtNom1.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private void remplirComboNom(ComboBox c) {
        try {
            List<Fournisseur> liste = fournisseurDAO.listerFournisseurs();
            liste.forEach((m) -> {
                c.getItems().add(m.getNomFourn());
            });
        } catch (Exception e) {
        }
    }

    private void remplirCombo(ComboBox c) {
        try {
            List<Departement> liste = departementDAO.listerDepartements();
            liste.forEach((m) -> {
                c.getItems().add(m.getCodeDep());
            });
        } catch (Exception e) {
        }
    }

    private void remplirComboEmp(ComboBox c) {
        try {
            List<Service> liste = serviceDAO.listerServices();
            liste.forEach((m) -> {
                c.getItems().add(m.getNomServ());
            });
        } catch (Exception e) {
        }
    }

    private void remplirComboAgence(ComboBox c) {
        try {
            List<Agence> liste = agenceDAO.listerAgences();
            liste.forEach((m) -> {
                c.getItems().add(m.getNomAgence());
            });
        } catch (Exception e) {
        }
    }

    @FXML
    private void recupererType(ActionEvent event) {
        switch (cmbTypeImmo.getSelectionModel().getSelectedIndex()) {
            case 0:
                cmbCompteImmo.getSelectionModel().select("24100");
                break;
            case 1:
                cmbCompteImmo.getSelectionModel().select("24200");
                break;
            case 2:
                cmbCompteImmo.getSelectionModel().select("24300");
                break;
            case 3:
                cmbCompteImmo.getSelectionModel().select("24400");
                break;
            case 4:
                cmbCompteImmo.getSelectionModel().select("24500");
                break;
            case 5:
                cmbCompteImmo.getSelectionModel().select("24600");
                break;
    }

}
    }
