/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.BlacklistDAO;
import DAO.MembreDAO;
import DAOFactory.DAOFactory;
import Models.Blacklist;
import Models.Membre;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class PageAuthentificationValidateurController implements Initializable {

    @FXML
    private Button btnLogin;
    @FXML
    private Hyperlink inscription;
    @FXML
    private Label message;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private TextField txtUsername;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.membreDAO = daoFactory.getMembreDAO();
        this.blacklistDAO = daoFactory.getBlacklistDAO();
    }
    MembreDAO membreDAO;
    BlacklistDAO blacklistDAO;

    DAOFactory daoFactory;

    @FXML
    private void btnLoginAction(ActionEvent event) {
        String nom = txtUsername.getText().trim();
        String mdp = txtPassword.getText().trim();

        if (!nom.isEmpty() && !mdp.isEmpty()) {
            Membre membre = membreDAO.rechercher(nom, mdp);
            if (membre != null) {
                if (membre.getStatut().equalsIgnoreCase("activé")) {
                    Blacklist blacklist = blacklistDAO.rechercherParId(membre.getId());
                    if (blacklist != null) {
                        message.setText("Connexion impossible! Vous etes dans le blacklist");
                    } else {
                        ((Node) (event.getSource())).getScene().getWindow().hide();

                        try {
                            FXMLLoader loader = new FXMLLoader();
                            loader.setLocation(getClass().getResource("/View/MenuValidateur.fxml"));
                            loader.load();
                            MenuValidateurController mc = loader.getController();
                            mc.txtIdSession.setText(String.valueOf(membre.getId()));
                            mc.txtEmailSession.setText(membre.getEmail());
                            mc.txtMdpSession.setText(membre.getMdp());
                            mc.txtNomSession.setText(membre.getNom());
                            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
                            Scene scene = new Scene(root);
                            Stage stage = new Stage();
                            stage.setScene(scene);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
                            stage.setTitle("Menu");
                            stage.sizeToScene();
                            stage.show();
                        } catch (IOException ex) {
                            System.out.println(ex);
                        }
                    }
                } else {
                    message.setText("Votre compte est desactivé");
                }
            } else {
                message.setText("Nom d'utilisateur ou mot de passse incorrect");
            }
        } else {
            message.setText("Remplissez tous les champs");
        }

        
    }

    @FXML
    private void open_registration(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/Inscription.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("Inscription");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.sizeToScene();
        stage.show();
    }

}
