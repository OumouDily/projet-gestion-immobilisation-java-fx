/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.FactureDAO;
import DAO.FournisseurDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Facture;
import Models.Fournisseur;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class ModifFournisseurController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private TableView<Fournisseur> tableFournisseur;
    @FXML
    private TableColumn<Fournisseur, String> numFiscal;
    @FXML
    private TableColumn<Fournisseur, String> nomFourni;
    @FXML
    private TableColumn<Fournisseur, String> adress;
     @FXML
    private TableColumn<Fournisseur, String> prenomFourni;
    @FXML
    private TextField txtNomFourni;
    private TextField txtPrenom;
    @FXML
    private TextField txtAdresse;
    @FXML
    private TextField txtNumero;
    @FXML
    private Button btnValider;
   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.fournisseurDAO = daoFactory.getFournisseurDAO();

        numFiscal.setCellValueFactory(new PropertyValueFactory<>("numFiscal"));
        nomFourni.setCellValueFactory(new PropertyValueFactory<>("nomFourn"));
        prenomFourni.setCellValueFactory(new PropertyValueFactory<>("prenomFourn"));
        adress.setCellValueFactory(new PropertyValueFactory<>("adresse"));
        listeFournisseur();
    }

    FournisseurDAO fournisseurDAO;

    Message message;
    DAOFactory daoFactory;
    ObservableList<Fournisseur> data = FXCollections.observableArrayList();

    @FXML
    private void retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/modification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    private void listeFournisseur() {
        tableFournisseur.getItems().clear();
        List<Fournisseur> liste = fournisseurDAO.listerFournisseurs();
        liste.forEach((m) -> {
//            Integer id = m.getId();
           
            String nomFourn = m.getNomFourn();
            String numFiscal = m.getNumFiscal();
            String adresse = m.getAdresse();
            data.add(m);
        });
        tableFournisseur.refresh();
        tableFournisseur.setItems(data);
    }

    @FXML
    private void valider(ActionEvent event) {
        String nomFourn = txtNomFourni.getText();
       
        String numFiscal = txtNumero.getText();
        String adresse = txtAdresse.getText();

        if (!nomFourn.isEmpty() && !numFiscal.isEmpty()&& !adresse.isEmpty()) {
            Fournisseur fournisseur = fournisseurDAO.rechercherParId(idF);
            if (fournisseur != null) {
                fournisseur.setNomFourn(nomFourn);
               
                fournisseur.setNumFiscal(numFiscal);
                fournisseur.setAdresse(adresse);

                if (fournisseurDAO.modifierFournisseur(fournisseur)) {
                    listeFournisseur();
                    message = new Message("INFORMATION", "INFORMATION", "Fournisseur", "Modifié avec succès");
                  
                    txtNomFourni.clear();
                    txtNumero.clear();
                    txtAdresse.clear();

                } else {
                    message = new Message("ERROR", "Erreur", "Fournisseur", "Echec de la modification");
                }

            }

        } else {
            message = new Message("ERROR", "Erreur", "INFORMATION", "Au moins un des champs est vide");
        }
    }
    ObservableList<Fournisseur> ligneSelectionner = null;
    Integer idF;

    @FXML
    private void selectionnerFournisseur(MouseEvent event) {
        ligneSelectionner = tableFournisseur.getSelectionModel().getSelectedItems();
        ligneSelectionner.forEach((ls) -> {
            Fournisseur four = fournisseurDAO.rechercherParNomFournisseur(ls.getNomFourn());
            idF = four.getId();
            txtNomFourni.setText(ls.getNomFourn());
          
            txtAdresse.setText(ls.getAdresse());    
            txtNumero.setText(ls.getNumFiscal());
        });
    }

}
