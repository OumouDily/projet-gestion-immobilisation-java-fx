/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class MenuController implements Initializable {

    @FXML
    private Hyperlink deconnexion;
    @FXML
    private Button saisie;
    @FXML
    private Button administrer;
    @FXML
    private Button recherche;
    @FXML
    private Button rechercheFacture;
    @FXML
    private Button modifier;
    @FXML
    private Button modifierProfil;
    @FXML
    private Button vente;
    @FXML
    public TextField txtIdSession;
    @FXML
    public TextField txtNomSession;
    @FXML
    public TextField txtEmailSession;
    @FXML
    public TextField txtMdpSession;
    @FXML
    private Label message;
    @FXML
    private Button btnValider;
    @FXML
    public TextField txtNom;
    @FXML
    public TextField txtEmail;
    @FXML
    public TextField txtMdp;
    @FXML
    public TextField txtStatutSession;
    @FXML
    public TextField txtRangSession;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void saisir(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/Saisie.fxml"));
            loader.load();
            SaisieController mpc = loader.getController();
            mpc.txtIdSession.setText(txtIdSession.getText());
            mpc.txtEmail.setText(txtEmailSession.getText());
            mpc.txtMdp.setText(txtMdpSession.getText());
            mpc.txtNom1.setText(txtNomSession.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @FXML
    private void Administrer(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/listeMembres.fxml"));
            loader.load();
            ListeMembresController mpc = loader.getController();
            mpc.txtIdSession.setText(txtIdSession.getText());
            mpc.txtEmail1.setText(txtEmailSession.getText());
            mpc.txtMdp.setText(txtMdpSession.getText());
            mpc.txtNom1.setText(txtNomSession.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @FXML
    private void Modifier(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/modification.fxml"));
            loader.load();
            ModificationController mpc = loader.getController();
            mpc.txtIdSession.setText(txtIdSession.getText());
            mpc.txtEmail.setText(txtEmailSession.getText());
            mpc.txtMdp.setText(txtMdpSession.getText());
            mpc.txtNom.setText(txtNomSession.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @FXML
    private void Rechercher(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/rechercher.fxml"));
            loader.load();
            RechercherController mpc = loader.getController();
            mpc.txtIdSession.setText(txtIdSession.getText());
            mpc.txtEmail.setText(txtEmailSession.getText());
            mpc.txtMdp.setText(txtMdpSession.getText());
            mpc.txtNom.setText(txtNomSession.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @FXML
    private void RechercherFacture(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/facture.fxml"));
            loader.load();
            FactureController mpc = loader.getController();
            mpc.txtIdSession.setText(txtIdSession.getText());
            mpc.txtEmail.setText(txtEmailSession.getText());
            mpc.txtMdp.setText(txtMdpSession.getText());
            mpc.txtNom.setText(txtNomSession.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @FXML
    private void vendre(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/vente.fxml"));
            loader.load();
            VenteController mpc = loader.getController();
            mpc.txtIdSession.setText(txtIdSession.getText());
            mpc.txtEmail.setText(txtEmailSession.getText());
            mpc.txtMdp.setText(txtMdpSession.getText());
            mpc.txtNom.setText(txtNomSession.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @FXML
    private void ModifierProfil(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/modifierProfil.fxml"));
            loader.load();
            ModifierProfilController mpc = loader.getController();
            mpc.txtIdSession.setText(txtIdSession.getText());
            mpc.txtEmail.setText(txtEmailSession.getText());
            mpc.txtMdp.setText(txtMdpSession.getText());
            mpc.txtNom.setText(txtNomSession.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @FXML
    private void Valider(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/listeEquipements.fxml"));
            loader.load();
            ListeEquipementsController mpc = loader.getController();
            mpc.txtIdSession.setText(txtIdSession.getText());
            mpc.txtEmail.setText(txtEmailSession.getText());
            mpc.txtMdp.setText(txtMdpSession.getText());
            mpc.txtNom.setText(txtNomSession.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

  

}
