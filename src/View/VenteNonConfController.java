/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.EquipementDAO;
import DAO.VenteDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Equipement;
import Models.Membre;
import Models.Vente;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class VenteNonConfController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private TableView<Vente> listeVente;
    private TableColumn<Vente, String> codeVenteColumn;
    @FXML
    private TableColumn<Vente, String> typeImmoColumn;
    @FXML
    private TableColumn<Vente, String> nomImmoColumn;
    @FXML
    private TableColumn<Vente, Double> prixVenteColumn;
    @FXML
    private TableColumn<Vente, String> nomAcheteurColumn;
    private TextField txtNum;
    @FXML
    private TextField txtType;
    @FXML
    private TextField txtPrix;
    @FXML
    private TextField txtNom;
    @FXML
    private TextField txtUser;
    @FXML
    private Button btnVente;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();

        this.venteDAO = daoFactory.getVenteDAO();

//        codeVenteColumn.setCellValueFactory(new PropertyValueFactory<>("codeVente"));
        typeImmoColumn.setCellValueFactory(new PropertyValueFactory<>("typeImmo"));
        nomImmoColumn.setCellValueFactory(new PropertyValueFactory<>("nomImmo"));
        prixVenteColumn.setCellValueFactory(new PropertyValueFactory<>("prixVente"));
        nomAcheteurColumn.setCellValueFactory(new PropertyValueFactory<>("nomAcheteur"));

        lister();
    }
    Message message;

    VenteDAO venteDAO;
    DAOFactory daoFactory;
    ObservableList<Vente> data = FXCollections.observableArrayList();

    @FXML
    private void retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/vente.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }
    ObservableList<Vente> ligneSelectionner = null;

    @FXML
    private void selectionnerEquip(MouseEvent event) {
        ligneSelectionner = listeVente.getSelectionModel().getSelectedItems();
        ligneSelectionner.forEach((ls) -> {
//            txtNum.setText(ls.getCodeVente());
            txtType.setText(ls.getTypeImmo());
            txtNom.setText(ls.getNomImmo());
            txtPrix.setText(String.valueOf(ls.getPrixVente()));
            txtUser.setText(ls.getNomAcheteur());
        });
    }

    @FXML
    private void Vendre(ActionEvent event) {
        String nom = txtNom.getText().trim();
       
        String typeImmo = txtType.getText();
        String nomImmo = txtNom.getText();
        Double prixVente = Double.parseDouble(txtPrix.getText());
        String nomAcheteur = txtUser.getText();
        if (!typeImmo.isEmpty() && !nomImmo.isEmpty() && !nomAcheteur.isEmpty()) {
            Vente vente = venteDAO.rechercherParNom(nom);

            if (vente != null) {

                vente.setCodeVente(vente.getCodeVente());

                vente.setTypeImmo(vente.getTypeImmo());

                vente.setNomImmo(vente.getNomImmo());
                vente.setPrixVente(vente.getPrixVente());
                vente.setNomAcheteur(vente.getNomAcheteur());
                vente.setEtat("Vendu");

                if (venteDAO.modifierVente(vente)) {

                    message = new Message("INFORMATION", "INFORMATION", "Vente", "Vendu");

                } else {
                    message = new Message("ERROR", "Erreur", "Vente", "Echec de la vente");
                }
            } else {
                System.out.println("erreur");
            }

        } else {
//            message = new Message("ERROR", "Erreur", "Inscription", "Au moins un des champs est vide");
        }
    }

    private void lister() {
        listeVente.getItems().clear();
        List<Vente> liste = venteDAO.listerVenteNonConf();

        liste.forEach((m) -> {

//
////            String cde_equip = m.getCde_equip();
//            String typeImmo = m.getTypeImmo();
//            String nomImmo = m.getNomImmo();
//            Double prixVente = m.getPrixVente();
//            String nomAcheteur = m.getNomAcheteur();
            data.add(m);
        });
        listeVente.refresh();
        listeVente.setItems(data);
    }

}
