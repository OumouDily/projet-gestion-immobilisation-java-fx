/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.EquipementDAO;
import DAO.ServiceDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Equipement;
import Models.Service;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class RechercheServiceInitiateurController implements Initializable {

    @FXML
    private Hyperlink deconnexion;
    @FXML
    private Hyperlink retour;
    @FXML
    private ComboBox<String> listNom;
    @FXML
    private TableView<Equipement> tableEquip;
    private TableColumn<Equipement, String> cdeImmo;
    @FXML
    private TableColumn<Equipement, String> typeImmo;
    @FXML
    private TableColumn<Equipement, Date> dateMES;
    @FXML
    private TableColumn<Equipement, String> nomImmo;
    @FXML
    private TableColumn<Equipement, Double> prixAchat;
    @FXML
    private TableColumn<Equipement, String> nomUser;

    @FXML
    private TableColumn<Equipement, String> etatColumn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.serviceDAO = daoFactory.getServiceDAO();
        this.equipementDAO = daoFactory.getEquipementDAO();
        remplirCombo(listNom);
//        cdeImmo.setCellValueFactory(new PropertyValueFactory<>("cde_equip"));
        typeImmo.setCellValueFactory(new PropertyValueFactory<>("type_immo"));
        etatColumn.setCellValueFactory(new PropertyValueFactory<>("etat"));
        dateMES.setCellValueFactory(new PropertyValueFactory<>("date"));
        nomImmo.setCellValueFactory(new PropertyValueFactory<>("nom_equip"));
        prixAchat.setCellValueFactory(new PropertyValueFactory<>("montant"));
        nomUser.setCellValueFactory(new PropertyValueFactory<>("nom_agent"));

    }
    Message message;

    ServiceDAO serviceDAO;
    EquipementDAO equipementDAO;
    DAOFactory daoFactory;
    ObservableList<Equipement> data = FXCollections.observableArrayList();

    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentificationInitiateur.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/rechercherInitiateur.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();

    }

    private void remplirCombo(ComboBox c) {
        try {
            List<Service> liste = serviceDAO.listerServices();

            liste.forEach((m) -> {
                c.getItems().add(m.getNomServ());
            });
        } catch (Exception e) {
        }
    }



    @FXML
    private void Afficher(ActionEvent event) {
        String nomServ = listNom.getValue();
       
         tableEquip.getItems().clear();
        List<Equipement> liste = equipementDAO.listerEquipementsParService(nomServ);
        liste.forEach((ls) -> {

//        nomAgent=ls.getNomAgent();
         data.add(ls);
        });
         tableEquip.refresh();
        tableEquip.setItems(data);
    }
    
}
