/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.MembreDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Membre;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class InscriptionController implements Initializable {

    @FXML
    private Button valider;
    @FXML
    private TextField txtNom;
    @FXML
    private TextField txtMdp;
    @FXML
    private TextField txtEmail;
    @FXML
    private Hyperlink retour;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.membreDAO = daoFactory.getMembreDAO();
    }

    Message message;
    MembreDAO membreDAO;
    DAOFactory daoFactory;

    @FXML
    private void Recuperer(ActionEvent event) {

        String nom = txtNom.getText();
        String email = txtEmail.getText();
        String mdp = txtMdp.getText();
        String statut = "Désactivé";

        if (!nom.isEmpty() && !email.isEmpty() && !mdp.isEmpty()) {
            Membre membre = new Membre();
            membre.setNom(nom);
            membre.setEmail(email);
            membre.setMdp(mdp);
            membre.setStatut(statut);
            membre.setRang(1);
            if (membreDAO.Ajouter(membre)) {
                message = new Message("INFORMATION", "INFORMATION", "Inscription", "Enregistré avec succès");
            } else {
                message = new Message("ERROR", "Erreur", "Inscription", "Echec de l'enregistrement");
            }

        } else {
            message = new Message("ERROR", "Erreur", "Inscription", "Au moins un des champs est vide");
        }

    }

    @FXML
    private void Retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

        
        stage.setTitle("Inscription");

        stage.setScene(new Scene(root));
        stage.setResizable(false);
     
        stage.sizeToScene();
        stage.show();
    }

}
