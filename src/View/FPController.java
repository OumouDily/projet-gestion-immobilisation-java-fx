/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.FactureDAO;
import DAO.PaiementDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Facture;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class FPController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private TableView<Facture> tableFacture;
    @FXML
    private TableColumn<Facture, String> numFacture;
    @FXML
    private TableColumn<Facture, String> nomFour;
    @FXML
    private TableColumn<Facture, Date> dateFact;
    @FXML
    private TableColumn<Facture, Double> MT;
    @FXML
    private TableColumn<Facture, String> taxes;
    private TableColumn<Facture, Double> reste;
    @FXML
    private TableColumn<Facture, String> statutColumn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
          this.daoFactory = DAOFactory.getInstance();
        this.factureDAO = daoFactory.getFactureDAO();
        this.paiementDAO = daoFactory.getPaiementDAO();
        lister();
//        numFacture.setCellValueFactory(new PropertyValueFactory<>("numFacture"));
        nomFour.setCellValueFactory(new PropertyValueFactory<>("nomFournisseur"));
        dateFact.setCellValueFactory(new PropertyValueFactory<>("date"));
        MT.setCellValueFactory(new PropertyValueFactory<>("montantTotal"));
        taxes.setCellValueFactory(new PropertyValueFactory<>("taxe"));
        statutColumn.setCellValueFactory(new PropertyValueFactory<>("statutFacture"));
    }    
       Message message;
    FactureDAO factureDAO;
    PaiementDAO paiementDAO;
    DAOFactory daoFactory;
    ObservableList<Facture> data = FXCollections.observableArrayList();

    @FXML
    private void retourner(ActionEvent event) {
          ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/facture.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void deconnecter(ActionEvent event) {
         ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void SelectionnerFacture(MouseEvent event) {
    }
    private void lister() {
        
        tableFacture.getItems().clear();
        List<Facture> liste = factureDAO.listerFacturesPayees();
        liste.forEach((m) -> {
//            String nomFacture = m.getNomFacture();
//            Date date = m.getDate();
////            String numFacture = m.getNumFacture();
//            Double montant = m.getMontantTotal();
//            String taxe = m.getTaxe();
//            Double reste = m.getReste();
            data.add(m);
        });
        tableFacture.refresh();
        tableFacture.setItems(data);
    }
}
