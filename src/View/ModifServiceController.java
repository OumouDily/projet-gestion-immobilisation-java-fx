/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.ServiceDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Employe;
import Models.Service;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class ModifServiceController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private ComboBox<String> selectNom;
    @FXML
    private Button btnValider;
    @FXML
    private TextField txtCode;
    @FXML
    private TextField txtNom;
    @FXML
    private TextField txtDep;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.serviceDAO = daoFactory.getServiceDAO();
        remplirCombo(selectNom);
    }

    ServiceDAO serviceDAO;

    Message message;
    DAOFactory daoFactory;
    ObservableList<Service> data = FXCollections.observableArrayList();

    @FXML
    private void retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/modification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    private void remplirCombo(ComboBox c) {
        try {
            List<Service> liste = serviceDAO.listerServices();
            c.getItems().add("");
            liste.forEach((m) -> {
                c.getItems().add(m.getNomServ());
            });
        } catch (Exception e) {
        }
    }
    Integer id;

    @FXML
    private void selectionnerNom(ActionEvent event) {
        if (selectNom.getSelectionModel().getSelectedIndex() > 0) {
            Service serv = serviceDAO.rechercher(selectNom.getValue());
            if (serv != null) {
                id = serv.getId();
                txtCode.setText(serv.getCdeServ());
                txtNom.setText(serv.getNomServ());
                txtDep.setText(serv.getCdeDep());
            } else {
            }
        } else {
            txtCode.clear();
            txtNom.clear();
            txtDep.clear();
//            txtCode.clear();
        }
    }

    @FXML
    private void modifierService(ActionEvent event) {
        String cdeServ = txtCode.getText();
        String nomServ = txtNom.getText();
        String cdeDep = txtDep.getText();

        if (!cdeServ.isEmpty() && !nomServ.isEmpty()&& !cdeDep.isEmpty()) {
            Service service = serviceDAO.rechercherParId(id);
            if (service != null) {
                service.setCdeServ(cdeServ);
                service.setNomServ(nomServ);
                service.setCdeDep(cdeDep);
               
                if (serviceDAO.modifierService(service)) {
                    message = new Message("INFORMATION", "INFORMATION", "Service", "Modifié avec succès");
                } else {
                    message = new Message("ERROR", "Erreur", "Service", "Echec de la modification");
                }

            }

        } else {
            message = new Message("ERROR", "Erreur", "INFORMATION", "Au moins un des champs est vide");
        }
    }

}
