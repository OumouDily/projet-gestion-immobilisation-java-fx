/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class ModificationController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private Button txtEmploye;
    @FXML
    private Button txtFacture;
    @FXML
    private Button txtFournisseur;
    @FXML
    private Button txtService;
    @FXML
    public TextField txtIdSession;
    @FXML
    public TextField txtNomSession;
    @FXML
    public TextField txtEmailSession;
    @FXML
    public TextField txtMdpSession;
    @FXML
    public TextField txtNom;
    @FXML
    public TextField txtEmail;
    @FXML
    public TextField txtMdp;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void retourner(ActionEvent event) {
    ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/Menu.fxml"));
            loader.load();
            MenuController mc = loader.getController();
            mc.txtIdSession.setText(txtIdSession.getText());
            mc.txtEmailSession.setText(txtEmail.getText());
            mc.txtMdpSession.setText(txtMdp.getText());
            mc.txtNomSession.setText(txtNom.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
    

    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void modifEmploye(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/modifEmploye.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void modifFacture(ActionEvent event) {
         ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/modifFacture.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void modifFournisseur(ActionEvent event) {
         ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/modifFournisseur.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void modifService(ActionEvent event) {
         ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/modifService.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    
}
