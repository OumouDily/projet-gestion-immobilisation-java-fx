/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.FactureDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Facture;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import static jdk.nashorn.internal.runtime.Debug.id;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class ModifFactureController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private TableView<Facture> tableFacture;
    @FXML
    private TableColumn<Facture, String> ref;
    @FXML
    private TableColumn<Facture, Date> dateF;
    @FXML
    private TableColumn<Facture, String> four;
    @FXML
    private TableColumn<Facture, Double> mnt;
    @FXML
    private TextField txtRef;
    @FXML
    private DatePicker txtDate;
    @FXML
    private TextField txtNom;
    @FXML
    private TextField txtMontant;
    @FXML
    private Button btnValider;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.factureDAO = daoFactory.getFactureDAO();

        ref.setCellValueFactory(new PropertyValueFactory<>("numFacture"));
        dateF.setCellValueFactory(new PropertyValueFactory<>("date"));
        four.setCellValueFactory(new PropertyValueFactory<>("nomFournisseur"));
        mnt.setCellValueFactory(new PropertyValueFactory<>("montantTotal"));
        listeFacture();

    }

    FactureDAO factureDAO;

    Message message;
    DAOFactory daoFactory;
    ObservableList<Facture> data = FXCollections.observableArrayList();

    @FXML
    private void retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/modification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    private void listeFacture() {
        tableFacture.getItems().clear();
        List<Facture> liste = factureDAO.listerFacturess();
        liste.forEach((m) -> {
//            Integer id = m.getId();
            String numFacture = m.getNumFacture();
            Date date = m.getDate();
            String nomFacture = m.getNomFournisseur();
            Double montantTotal = m.getMontantTotal();
            data.add(m);
        });
        tableFacture.refresh();
        tableFacture.setItems(data);
    }

    @FXML
    private void valider(ActionEvent event) {
       
        String numFacture = txtRef.getText();
        LocalDate ld = txtDate.getValue();
        Date sqlDate = Date.valueOf(ld);
        String nomFournisseur = txtNom.getText();
        Double montantTotal = Double.parseDouble(txtMontant.getText().trim());
     

        if (!numFacture.isEmpty() && !nomFournisseur.isEmpty()) {
            Facture facture = factureDAO.rechercherParId(idF);
            if (facture != null) {
               
                facture.setNumFacture(numFacture);
                facture.setDate(sqlDate);
                facture.setNomFournisseur(nomFournisseur);
                facture.setMontantTotal(facture.getMontantTotal());
//                facture.setTaxe(null);
//                facture.setStatutFacture(null);

                if (factureDAO.modifierFacture(facture)) {
                    listeFacture();
                    message = new Message("INFORMATION", "INFORMATION", "Facture", "Modifié avec succès");
                    txtRef.clear();
//                    txtDate.clear();
                     txtDate.setValue(null);
                    txtNom.clear();
                    txtMontant.clear();
                    
                } else {
                    message = new Message("ERROR", "Erreur", "Employe", "Echec de la modification");
                }

            }

        } else {
            message = new Message("ERROR", "Erreur", "INFORMATION", "Au moins un des champs est vide");
        }
    }
    ObservableList<Facture> ligneSelectionner = null;
    Integer idF;

    @FXML
    private void selectionnerFacture(MouseEvent event) {
        ligneSelectionner = tableFacture.getSelectionModel().getSelectedItems();
        ligneSelectionner.forEach((ls) -> {
            Facture fac = factureDAO.rechercherParNomFournisseur(ls.getNomFournisseur());
            txtRef.setText(ls.getNumFacture());
            idF = fac.getId();

            txtDate.setValue(LocalDate.parse(String.valueOf(ls.getDate())));
            txtNom.setText(ls.getNomFournisseur());
            txtMontant.setText(String.valueOf(ls.getMontantTotal()));
            

        });
    }

}
