/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.EmployeDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Employe;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class ModifEmployeController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private ComboBox<String> selectNom;
    @FXML
    private TextField txtNom;
    private TextField txtCode;
    @FXML
    private Button btnValiderModif;
    private TextField txtCodeAg;
    @FXML
    private TextField txtPrenom;
    @FXML
    private TextField txtIdSession;
    @FXML
    private TextField txtNomSession;
    @FXML
    private TextField txtEmailSession;
    @FXML
    private TextField txtMdpSession;
    @FXML
    private TextField txtNom1;
    @FXML
    private TextField txtEmail;
    @FXML
    private TextField txtMdp;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.employeDAO = daoFactory.getEmployeDAO();
        remplirCombo(selectNom);
    }

    EmployeDAO employeDAO;

    Message message;
    DAOFactory daoFactory;
    ObservableList<Employe> data = FXCollections.observableArrayList();

    @FXML
    private void retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/modification.fxml"));
            loader.load();
            ModificationController mc = loader.getController();
            mc.txtIdSession.setText(txtIdSession.getText());
            mc.txtEmailSession.setText(txtEmail.getText());
            mc.txtMdpSession.setText(txtMdp.getText());
            mc.txtNomSession.setText(txtNom.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void modifierEmp(ActionEvent event) {

        String nomAgent = txtNom.getText();
        String prenomAgent = txtPrenom.getText();

        if (!nomAgent.isEmpty() && !prenomAgent.isEmpty()) {
            Employe employe = employeDAO.rechercherParId(idAgent);
            if (employe != null) {
                employe.setCdeAgent(employe.getCdeAgent());
                employe.setNomAgent(nomAgent);
                employe.setPrenomAgent(prenomAgent);
                employe.setNomServ(employe.getNomServ());
                employe.setNomAgence(employe.getNomAgence());
                if (employeDAO.modifierEmploye(employe)) {
                    message = new Message("INFORMATION", "INFORMATION", "Employe", "Votre compte a été modifié");
                } else {
                    message = new Message("ERROR", "Erreur", "Employe", "Echec de la modification");
                }

            }

        } else {
            message = new Message("ERROR", "Erreur", "INFORMATION", "Au moins un des champs est vide");
        }
    }

    private void remplirCombo(ComboBox c) {
        try {
            List<Employe> liste = employeDAO.listerEmployes();
            c.getItems().add("");
            liste.forEach((m) -> {
                c.getItems().add(m.getNomAgent());
            });
        } catch (Exception e) {
        }
    }
    Integer idAgent;

    @FXML
    private void selectionnerNom(ActionEvent event) {
        if (selectNom.getSelectionModel().getSelectedIndex() > 0) {
            Employe emp = employeDAO.rechercher(selectNom.getValue());
            if (emp != null) {
                idAgent = emp.getId();
                txtNom.setText(emp.getNomAgent());
                txtPrenom.setText(emp.getPrenomAgent());
//                txtCode.setText(emp.getCdeServ());
            } else {
            }
        } else {
            txtNom.clear();
            txtPrenom.clear();
//            txtCode.clear();
        }
    }
}
