/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.VenteDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Vente;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class ListeVenduController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private TableView<Vente> listeVente;
    private TableColumn<Vente, String> codeVenteColumn;
    @FXML
    private TableColumn<Vente, String> typeImmoColumn;
    @FXML
    private TableColumn<Vente, String> nomImmoColumn;
    @FXML
    private TableColumn<Vente, Double> prixVenteColumn;
    @FXML
    private TableColumn<Vente, String> nomAcheteurColumn;
    @FXML
    private TableColumn<Vente, String> etatColumn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();

        this.venteDAO = daoFactory.getVenteDAO();

//        codeVenteColumn.setCellValueFactory(new PropertyValueFactory<>("codeVente"));
        typeImmoColumn.setCellValueFactory(new PropertyValueFactory<>("typeImmo"));
        nomImmoColumn.setCellValueFactory(new PropertyValueFactory<>("nomImmo"));
        prixVenteColumn.setCellValueFactory(new PropertyValueFactory<>("prixVente"));
        nomAcheteurColumn.setCellValueFactory(new PropertyValueFactory<>("nomAcheteur"));
        etatColumn.setCellValueFactory(new PropertyValueFactory<>("etat"));
        lister("Vendu");
    }

    Message message;

    VenteDAO venteDAO;
    DAOFactory daoFactory;
    ObservableList<Vente> data = FXCollections.observableArrayList();

    @FXML
    private void retourner(ActionEvent event) {
         ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/vente.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void deconnecter(ActionEvent event) {
         ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void selectionnerEquip(MouseEvent event) {
    }

     private void lister(String etat) {
        listeVente.getItems().clear();
        List<Vente> liste = venteDAO.listerEquipVendu(etat);
        liste.forEach((m) -> {
            
            data.add(m);
        });
        listeVente.refresh();
        listeVente.setItems(data);
    }
     
}
