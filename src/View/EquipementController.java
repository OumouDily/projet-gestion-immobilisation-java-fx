/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.EmployeDAO;
import DAO.EquipementDAO;
import DAO.FactureDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Employe;
import Models.Equipement;
import Models.Facture;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class EquipementController implements Initializable {

    @FXML
    public TextField txtCodeEquip;
    @FXML
    private TextField txtNomEquip;
    @FXML
    public TextField txtCompte;
    @FXML
    private TextField txtPrix;
    private TextField txtEtat;
    private TextField txtTaux;

    private TextField txtNomUser;
    @FXML
    private Button btnValiderEquip;
    @FXML
    private ComboBox<String> listAgent;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private Hyperlink retour;
    @FXML
    private DatePicker dateAff;
    @FXML
    private ComboBox<String> listEtat;
    @FXML
    private ComboBox<Double> TauxAmort;
    @FXML
    private TextField txtTypeSession;
    @FXML
    private TextField txtCompteSession;
    @FXML
    private ComboBox<Integer> cmbReference;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.equipementDAO = daoFactory.getEquipementDAO();
        this.employeDAO = daoFactory.getEmployeDAO();
         this.factureDAO = daoFactory.getFactureDAO();
        remplirCombo(listAgent);
        remplirComboRef(cmbReference);
        ObservableList<String> etatE = FXCollections.observableArrayList("BON ETAT", "MOYEN ETAT", "MAUVAIS ETAT", "PERDU");
        ObservableList<Double> tauxA = FXCollections.observableArrayList(0.1, 0.2, 0.33);
        listEtat.setItems(etatE);
        TauxAmort.setItems(tauxA);
    }
    Message message;
    EquipementDAO equipementDAO;
    EmployeDAO employeDAO;
    FactureDAO factureDAO;
    DAOFactory daoFactory;

//        listEtat.getSelectionModel().selectFirst();
    @FXML
    private void EnregistrerEquip(ActionEvent event) {
//        String cde_equip;
        String type_immo = txtCodeEquip.getText();
        String nom_equip = txtNomEquip.getText();
        Integer compte = Integer.parseInt(txtCompte.getText().trim());
        Double montant = Double.parseDouble(txtPrix.getText().trim());
//        equipement.setTaux_amort(Double.parseDouble(taux_amort));
        String etat = listEtat.getValue();
        Double taux_amort = TauxAmort.getValue();
        LocalDate ld = dateAff.getValue();
        Date sqlDate = Date.valueOf(ld);
//         String date = dateAff.getD
        String nom_agent = listAgent.getValue();
        Integer idReferenceFacture=cmbReference.getValue();
        if (!type_immo.isEmpty() && !nom_equip.isEmpty()) {
            Equipement equipement = new Equipement();
            equipement.setCde_equip(null);
            equipement.setDescrcpte(null);
            equipement.setValidation("Non validé");
            equipement.setR_C(null);
            equipement.setType_immo(type_immo);
            equipement.setNom_equip(nom_equip);
            equipement.setCompte(compte);
            equipement.setMontant(montant);
            equipement.setEtat(etat);
            equipement.setTaux_amort(taux_amort);
            equipement.setDate(sqlDate);
            equipement.setNom_agent(nom_agent);
            equipement.setIdReferenceFacture(idReferenceFacture);
            try {
                Employe emp = employeDAO.rechercher(String.valueOf(listAgent.getValue()));
                if (emp != null) {
                    equipement.setNom_agent(emp.getNomAgent());
                } else {
//                    System.out.println("erreur");
                }
            } catch (Exception e) {
                System.out.println(e);
            }
            if (equipementDAO.Ajouter(equipement)) {
                message = new Message("INFORMATION", "INFORMATION", "Enregistrement", "Enregistré avec succès");
                txtCodeEquip.clear();
                txtNomEquip.clear();
                txtCompte.clear();
                txtPrix.clear();
            } else {
                message = new Message("ERROR", "Erreur", "Enregistrement", "Echec de l'enregistrement");
            }
        } else {
            message = new Message("ERROR", "Erreur", "Enregistrement", "Au moins un des champs est vide");
        }
    }

    private void remplirCombo(ComboBox c) {
        try {
            List<Employe> liste = employeDAO.listerEmployes();
            liste.forEach((m) -> {
                c.getItems().add(m.getNomAgent());
            });
        } catch (Exception e) {
        }
    }

    private void remplirComboRef(ComboBox c) {
        try {
            List<Facture> liste = factureDAO.listerFactures();
            liste.forEach((m) -> {
                c.getItems().add(m.getReference());
            });
        } catch (Exception e) {
        }
    }
    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/Saisie.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.sizeToScene();
        stage.show();
    }

}
