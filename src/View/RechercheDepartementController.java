/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.DepartementDAO;
import DAO.EquipementDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Departement;
import Models.Equipement;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class RechercheDepartementController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
 
    @FXML
    private TableView<Equipement> tableDep;
    private TableColumn<Equipement, String> cdeImmo;
    @FXML
    private TableColumn<Equipement, String> typeImmo;
    @FXML
    private TableColumn<Equipement, String> etatColumn;
    @FXML
    private TableColumn<Equipement, Date> dateMES;
    @FXML
    private TableColumn<Equipement, String> nomImmo;
    @FXML
    private TableColumn<Equipement, Double> prixAchat;
    @FXML
    private TableColumn<Equipement, String> nomUser;
    @FXML
    private ComboBox<String> nomDepList;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
          this.daoFactory = DAOFactory.getInstance();
        this.equipementDAO = daoFactory.getEquipementDAO();
        this.departementDAO = daoFactory.getDepartementDAO();
        remplirCombo(nomDepList);
      
        typeImmo.setCellValueFactory(new PropertyValueFactory<>("type_immo"));
        etatColumn.setCellValueFactory(new PropertyValueFactory<>("etat"));
        dateMES.setCellValueFactory(new PropertyValueFactory<>("date"));
        nomImmo.setCellValueFactory(new PropertyValueFactory<>("nom_equip"));
        prixAchat.setCellValueFactory(new PropertyValueFactory<>("montant"));
        nomUser.setCellValueFactory(new PropertyValueFactory<>("nom_agent"));
    }    
     Message message;

    EquipementDAO equipementDAO;
    DepartementDAO departementDAO;
    DAOFactory daoFactory;
    ObservableList<Equipement> data = FXCollections.observableArrayList();
    @FXML
    private void retourner(ActionEvent event) {
         ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/rechercher.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void deconnecter(ActionEvent event) {
         ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void listNomDep(ActionEvent event) {
          String nomDep = nomDepList.getValue();
//        System.out.println(nomAgent);
        tableDep.getItems().clear();
        List<Equipement> liste = equipementDAO.listerEquipementsParDep(nomDep);
        liste.forEach((ls) -> {

            data.add(ls);
        });
        tableDep.refresh();
        tableDep.setItems(data);
    }
    private void remplirCombo(ComboBox c) {
        try {
            List<Departement> liste = departementDAO.listerDepartements();

            liste.forEach((m) -> {
                c.getItems().add(m.getNomDep());
            });
        } catch (Exception e) {
        }
    }

    private void listeEquipement() {
        tableDep.getItems().clear();
        List<Equipement> liste = equipementDAO.listerEquipements();
        liste.forEach((m) -> {

//            String cde_equip = m.getCde_equip();
            String type_immo = m.getType_immo();
            String etat = m.getEtat();
            Date date = m.getDate();
            String nom_equip = m.getNom_equip();
            Double montant = m.getMontant();
            data.add(m);
        });
        tableDep.refresh();
        tableDep.setItems(data);
    }
    
}
