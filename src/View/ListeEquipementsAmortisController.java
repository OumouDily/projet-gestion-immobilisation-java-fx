/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.EquipementDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Equipement;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class ListeEquipementsAmortisController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private TableView<Equipement> tableEtat;
    private TableColumn<Equipement, String> cdeImmo;
    @FXML
    private TableColumn<Equipement, String> typeImmo;
    @FXML
    private TableColumn<Equipement, String> etatColumn;
    @FXML
    private TableColumn<Equipement, Date> dateMES;
    @FXML
    private TableColumn<Equipement, String> nomImmo;
    @FXML
    private TableColumn<Equipement, Double> prixAchat;
    @FXML
    private TableColumn<Equipement, String> nomUser;
    @FXML
    private TextField txtIdSession;
    @FXML
    private TextField txtNomSession;
    @FXML
    private TextField txtEmailSession;
    @FXML
    private TextField txtMdpSession;
    @FXML
    private TextField txtNom;
    @FXML
    private TextField txtEmail;
    @FXML
    private TextField txtMdp;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
             this.daoFactory = DAOFactory.getInstance();
        this.equipementDAO = daoFactory.getEquipementDAO();
        lister();
//        cdeImmo.setCellValueFactory(new PropertyValueFactory<>("cde_equip"));
        typeImmo.setCellValueFactory(new PropertyValueFactory<>("type_immo"));
        etatColumn.setCellValueFactory(new PropertyValueFactory<>("etat"));
        dateMES.setCellValueFactory(new PropertyValueFactory<>("date"));
        nomImmo.setCellValueFactory(new PropertyValueFactory<>("nom_equip"));
        prixAchat.setCellValueFactory(new PropertyValueFactory<>("montant"));
        nomUser.setCellValueFactory(new PropertyValueFactory<>("nom_agent"));
    }    

     Message message;
    EquipementDAO equipementDAO;

    DAOFactory daoFactory;
    ObservableList<Equipement> data = FXCollections.observableArrayList();
    @FXML
    private void retourner(ActionEvent event) {
          ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/listeEquipements.fxml"));
            loader.load();
            ListeEquipementsController mc = loader.getController();
            mc.txtIdSession.setText(txtIdSession.getText());
            mc.txtEmailSession.setText(txtEmail.getText());
            mc.txtMdpSession.setText(txtMdp.getText());
            mc.txtNomSession.setText(txtNom.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @FXML
    private void deconnecter(ActionEvent event) {
         ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void selectionner(MouseEvent event) {
    }
//    String etat;
     private void lister() {
        tableEtat.getItems().clear();
        List<Equipement> liste = equipementDAO.listerEquipementsAmortis();
        liste.forEach((m) -> {


            data.add(m);
        });
        tableEtat.refresh();
        tableEtat.setItems(data);
    }
}
