/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.BlacklistDAO;
import DAO.MembreDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Blacklist;
import Models.Membre;
import Models.Service;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class ListeMembresController implements Initializable {

    @FXML
    private TableView<Membre> tableView;
    @FXML
    private TableColumn<Membre, Integer> idColumn;
    @FXML
    private TableColumn<Membre, String> nomColumn;
    @FXML
    private TableColumn<Membre, String> emailColumn;
    @FXML
    private TableColumn<Membre, Integer> rangColumn;
    @FXML
    private TextField txtId;
    @FXML
    private TextField txtNom;
    @FXML
    private TextField txtEmail;
    @FXML
    private Button btnActiver;
    @FXML
    private Button btnTransferer;
    @FXML
    private Button btnSupprimer;
    @FXML
    private Button btnDesactiver;
    @FXML
    private Hyperlink retour1;
    @FXML
    private Hyperlink deconnecter;
    @FXML
    public TextField txtIdSession;
    @FXML
    public TextField txtNomSession;
    @FXML
    public TextField txtEmailSession;
    @FXML
    public TextField txtMdpSession;
    @FXML
    public TextField txtNom1;
    @FXML
    public TextField txtEmail1;
    @FXML
    public TextField txtMdp;

    @FXML
    private Spinner<Integer> spRang;
    @FXML
    private Button btnModifier;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.membreDAO = daoFactory.getMembreDAO();
        this.blacklistDAO = daoFactory.getBlacklistDAO();
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        nomColumn.setCellValueFactory(new PropertyValueFactory<>("nom"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        rangColumn.setCellValueFactory(new PropertyValueFactory<>("rang"));
        listeMembre();

//        tableView.setItems(rangM);
        SpinnerValueFactory<Integer> rangM = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 3, 1);
        spRang.setValueFactory(rangM);

    }
    MembreDAO membreDAO;
    BlacklistDAO blacklistDAO;
    Message message;
    DAOFactory daoFactory;
    ObservableList<Membre> data = FXCollections.observableArrayList();

    @FXML
    private void retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/Menu.fxml"));
            loader.load();
            MenuController mc = loader.getController();
            mc.txtIdSession.setText(txtIdSession.getText());
            mc.txtEmailSession.setText(txtEmail1.getText());
            mc.txtMdpSession.setText(txtMdp.getText());
            mc.txtNomSession.setText(txtNom1.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
    ObservableList<Membre> ligneSelectionner = null;

    @FXML
    private void SelectionnerMembre(MouseEvent event) {

        ligneSelectionner = tableView.getSelectionModel().getSelectedItems();
        ligneSelectionner.forEach((ls) -> {
            txtId.setText(String.valueOf(ls.getId()));
            txtNom.setText(ls.getNom());
            txtEmail.setText(ls.getEmail());
//            spRang.setValue(ls.getRang());
            SpinnerValueFactory<Integer> rangM = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 3, ls.getRang());
            spRang.setValueFactory(rangM);

        });
    }

    @FXML
    private void S(MouseEvent event) {
    }

    private void listeMembre() {
        tableView.getItems().clear();
        List<Membre> liste = membreDAO.listerMembres();
        liste.forEach((m) -> {
            Integer id = m.getId();
            String nom = m.getNom();
            String email = m.getEmail();
            data.add(m);
        });
        tableView.refresh();
        tableView.setItems(data);
    }

    @FXML
    private void Supprimer(ActionEvent event) {

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText(null);
        alert.setContentText("Etes-vous sur de vouloir supprimer?");
        Optional<ButtonType> action = alert.showAndWait();

        if (action.get() == ButtonType.OK) {
            if (ligneSelectionner != null && !txtId.getText().isEmpty()) {
                Membre mb = membreDAO.rechercherParId(Integer.parseInt(txtId.getText()));
                if (mb != null) {
                    if (membreDAO.supprimerMembre(mb)) {
                        listeMembre();
                        txtId.clear();
                        txtNom.clear();
                        txtEmail.clear();

                        message = new Message("INFORMATION", "Information", "Suppression", "Succès");
                    } else {
                        message = new Message("ERROR", "Erreur", "Suppression", "Echèc");
                    }
                } else {
                    message = new Message("ERROR", "Erreur", "SGBD", "Problème");
                }
            } else {
                message = new Message("ERROR", "Erreur", "Supppression", "Aucune ligne sel...");
            }
        } else if (action.get() == ButtonType.CANCEL) {
        }

    }

    @FXML
    private void Deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void activer(ActionEvent event) {
        Integer id = Integer.parseInt(txtId.getText());
        String nomB = txtNom.getText();
        String emailB = txtEmail.getText();

        if (!nomB.isEmpty() && !emailB.isEmpty()) {
            Membre membre = membreDAO.rechercherParId(id);
//            membre.setId(id);
            if (membre != null) {

                membre.setNom(membre.getNom());
                membre.setEmail(membre.getEmail());
                membre.setMdp(membre.getMdp());
                membre.setStatut("Activé");
                if (membreDAO.modifierMembre(membre)) {
                    message = new Message("INFORMATION", "INFORMATION", "Membre", "Votre compte a été activé");

                } else {
                    message = new Message("ERROR", "Erreur", "Membre", "Echec de l'activation");
                }
            } else {
                System.out.println("erreur");
            }

        } else {
//            message = new Message("ERROR", "Erreur", "Inscription", "Au moins un des champs est vide");
        }
    }

    @FXML
    private void transferer(ActionEvent event) {
        Integer idB = Integer.parseInt(txtId.getText());
        Integer idMembre = Integer.parseInt(txtId.getText());
        String nomB = txtNom.getText();
        String emailB = txtEmail.getText();

//        String mdp = txtMdp.getText();
        if (!nomB.isEmpty() && !emailB.isEmpty()) {
            Blacklist blacklist = new Blacklist();
            blacklist.setIdB(idB);
            blacklist.setIdMembre(idMembre);
            Membre mb = membreDAO.rechercherParId(Integer.parseInt(txtId.getText()));

            if (blacklistDAO.Ajouter(blacklist)) {
                message = new Message("INFORMATION", "INFORMATION", "Ajouter au blacklist", "Ajouté");
//                membreDAO.supprimerMembre(mb);
                listeMembre();

            } else {
                message = new Message("ERROR", "Erreur", "INFORMATION", "Echec de l'envoie");
            }

        } else {
//            message = new Message("ERROR", "Erreur", "Inscription", "Au moins un des champs est vide");
        }
    }

    @FXML
    private void desactiver(ActionEvent event) {
        Integer id = Integer.parseInt(txtId.getText());
        String nomB = txtNom.getText();
        String emailB = txtEmail.getText();

        if (!nomB.isEmpty() && !emailB.isEmpty()) {
            Membre membre = membreDAO.rechercherParId(id);
//            membre.setId(id);
            if (membre != null) {

                membre.setNom(membre.getNom());
                membre.setEmail(membre.getEmail());
                membre.setMdp(membre.getMdp());
                membre.setStatut("Désactivé");
                if (membreDAO.modifierMembre(membre)) {
                    message = new Message("INFORMATION", "INFORMATION", "Membre", "Votre compte a été désactivé");

                } else {
                    message = new Message("ERROR", "Erreur", "Membre", "Echec de la désactivation");
                }
            } else {
                System.out.println("erreur");
            }

        } else {
//            message = new Message("ERROR", "Erreur", "Inscription", "Au moins un des champs est vide");
        }
    }

    @FXML
    private void modifier(ActionEvent event) {
        Integer id = Integer.parseInt(txtId.getText());
        String nom = txtNom.getText();
        String email = txtEmail.getText();
        Integer rang = spRang.getValue();

        if (!nom.isEmpty() && !email.isEmpty()) {
            Membre membre = membreDAO.rechercherParId(id);
            if (membre != null) {
                membre.setId(id);
                membre.setNom(nom);
                membre.setEmail(email);
                membre.setRang(rang);

                if (membreDAO.modifierMembre(membre)) {
                    Membre mb = membreDAO.rechercherParId(id);
                    Integer idS = mb.getId();
                    String nomS = mb.getNom();
                    String emailS = mb.getEmail();
                    String mdpS = mb.getMdp();
                    String statutS = mb.getStatut();
                    Integer rangS = mb.getRang();
                    message = new Message("INFORMATION", "INFORMATION", "Membre", "Modifié avec succès");
                    listeMembre();
                    if (spRang.getValue() == 1) {
                        ((Node) (event.getSource())).getScene().getWindow().hide();
                        try {
                            FXMLLoader loader = new FXMLLoader();
                            loader.setLocation(getClass().getResource("/View/Menu.fxml"));
                            loader.load();
                            MenuController mpc = loader.getController();
                            mpc.txtIdSession.setText(String.valueOf(idS));
                            mpc.txtNomSession.setText(nomS);
                            mpc.txtEmailSession.setText(emailS);
                            mpc.txtMdpSession.setText(mdpS);
                            mpc.txtStatutSession.setText(statutS);
                            mpc.txtRangSession.setText(String.valueOf(rangS));

                            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
                            Scene scene = new Scene(root);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
                            stage.setTitle("");
                            stage.sizeToScene();
                            stage.show();
                        } catch (IOException ex) {
                            System.out.println(ex);
                        }

                    } else if (spRang.getValue() == 2) {
                        ((Node) (event.getSource())).getScene().getWindow().hide();
                        try {
                            FXMLLoader loader = new FXMLLoader();
                            loader.setLocation(getClass().getResource("/View/MenuValidateur.fxml"));
                            loader.load();
                            MenuValidateurController mpc = loader.getController();
                            mpc.txtIdSession.setText(String.valueOf(idS));
                            mpc.txtNomSession.setText(nomS);
                            mpc.txtEmailSession.setText(emailS);
                            mpc.txtMdpSession.setText(mdpS);
//                            mpc.txtStatutSession.setText(statutS);
//                            mpc.txtRangSession.setText(String.valueOf(rangS));

                            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
                            Scene scene = new Scene(root);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
                            stage.setTitle("");
                            stage.sizeToScene();
                            stage.show();
                        } catch (IOException ex) {
                            System.out.println(ex);
                        }

                    } else if(spRang.getValue() == 3){
                        ((Node) (event.getSource())).getScene().getWindow().hide();
                        try {
                            FXMLLoader loader = new FXMLLoader();
                            loader.setLocation(getClass().getResource("/View/MenuInitiateur.fxml"));
                            loader.load();
                            MenuInitiateurController mpc = loader.getController();
                            mpc.txtIdSession.setText(String.valueOf(idS));
                            mpc.txtNomSession.setText(nomS);
                            mpc.txtEmailSession.setText(emailS);
                            mpc.txtMdpSession.setText(mdpS);
//                            mpc.txtStatutSession.setText(statutS);
//                            mpc.txtRangSession.setText(String.valueOf(rangS));

                            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
                            Scene scene = new Scene(root);
                            Stage stage = new Stage();
                            stage.setScene(scene);
                            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
                            stage.setTitle("");
                            stage.sizeToScene();
                            stage.show();
                        } catch (IOException ex) {
                            System.out.println(ex);
                        }
                    }

                } else {
                    message = new Message("ERROR", "Erreur", "Membre", "Echec de la modification");
                }

            }

        } else {
            message = new Message("ERROR", "Erreur", "INFORMATION", "Au moins un des champs est vide");
        }
    }

}
