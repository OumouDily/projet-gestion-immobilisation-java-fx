/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import DAO.FactureDAO;
import DAO.PaiementDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Facture;
import Models.Paiement;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class FactureAPayerController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private TableView<Facture> tableFacture;
    @FXML
    private TableColumn<Facture, String> numFacture;
    @FXML
    private TableColumn<Facture, String> nomFour;
    @FXML
    private TableColumn<Facture, Date> dateFact;
    @FXML
    private TableColumn<Facture, Double> MT;
    @FXML
    private TableColumn<Facture, String> taxes;
    @FXML
    private TextField reste;
    @FXML
    private TextField txtNom;
    @FXML
    private TextField txtDate;
    @FXML
    private TextField txtMontant;
    @FXML
    private TextField txtTaxe;
    @FXML
    private DatePicker dateP;
    @FXML
    private TextField montantP;
    @FXML
    private ComboBox<String> typeP;
    @FXML
    private Button btnPayer;
    @FXML
    private TextField referencee;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.factureDAO = daoFactory.getFactureDAO();
        this.paiementDAO = daoFactory.getPaiementDAO();
        lister();
        numFacture.setCellValueFactory(new PropertyValueFactory<>("numFacture"));
        nomFour.setCellValueFactory(new PropertyValueFactory<>("nomFournisseur"));
        dateFact.setCellValueFactory(new PropertyValueFactory<>("date"));
        MT.setCellValueFactory(new PropertyValueFactory<>("montantTotal"));
        taxes.setCellValueFactory(new PropertyValueFactory<>("taxe"));
//        reste.setCellValueFactory(new PropertyValueFactory<>("reste"));
        ObservableList<String> listType = FXCollections.observableArrayList("Virement", "Chèque");
        typeP.setItems(listType);

    }
    Message message;
    FactureDAO factureDAO;
    PaiementDAO paiementDAO;
    DAOFactory daoFactory;
    ObservableList<Facture> data = FXCollections.observableArrayList();

    @FXML
    private void retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/facture.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    ObservableList<Facture> ligneSelectionner = null;

    String codeFact;

    Double mt;

    Double map;

    @FXML
    private void SelectionnerFacture(MouseEvent event) {

        ligneSelectionner = tableFacture.getSelectionModel().getSelectedItems();

        ligneSelectionner.forEach((ls) -> {
            
            codeFact = ls.getNumFacture();
            System.out.println(codeFact);
            map = ls.getMontantTotal();
            txtNom.setText(ls.getNomFournisseur());
            txtDate.setText(String.valueOf(ls.getDate()));
            txtMontant.setText(String.valueOf(ls.getMontantTotal()));
            mt = ls.getMontantTotal();
            txtTaxe.setText(ls.getTaxe());
        });
        try {
            Paiement p = paiementDAO.rechercherParCode(codeFact);
            if (p != null) {
                reste.setText(String.valueOf(map - p.getMontantPaye()) + " F CFA");

            } else {
                reste.setText(map + " F CFA");
            }
        } catch (Exception e) {
        }
    }

    @FXML
    private void Payer(ActionEvent event) {

        LocalDate ld = dateP.getValue();
        Date sqlDate = Date.valueOf(ld);
        String reference = referencee.getText();
        Double montantPaye = Double.parseDouble(montantP.getText().trim());
        String typePaiement = typeP.getValue();

        if (!reference.isEmpty()) {
            if (montantPaye <= mt) {
                Double r = mt - montantPaye;
                if (r >= 0) {
                    Paiement paiement = new Paiement();

                    paiement.setDateP(sqlDate);
                    paiement.setReference(reference);
                    paiement.setMontantPaye(montantPaye);
                    paiement.setTypePaiement(typePaiement);
                    paiement.setCdeFacture(codeFact);
                    
                    
                 

                    if (paiementDAO.Ajouter(paiement)) {
                        System.out.println("ok");
//                    System.out.println("Reste : "+r);

                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("Information");
                        alert.setHeaderText(null);
                        alert.setContentText("Facture payée! Montant restant\n" + r);
                        Optional<ButtonType> action = alert.showAndWait();

                        if (action.get() == ButtonType.OK) {

                        } else if (action.get() == ButtonType.CANCEL) {

                        }

                    } else {
                        System.out.println("ko");
                    }

                } else {
//                System.out.println("montant supérieur");

                }

            } else if (montantPaye > mt) {
                Double r = mt - montantPaye;
                if (r > 0) {

                    Paiement paiement = new Paiement();
//                paiement.setNumFacture(numFacture);
                    paiement.setDateP(sqlDate);
                    paiement.setReference(reference);
                    paiement.setMontantPaye(montantPaye);
                    paiement.setTypePaiement(typePaiement);
                    paiement.setCdeFacture(codeFact);
                    if (paiementDAO.Ajouter(paiement)) {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("Information");
                        alert.setHeaderText(null);
                        alert.setContentText("Facture payée! On doit vous retourner\n" + r);
                        Optional<ButtonType> action = alert.showAndWait();

                        if (action.get() == ButtonType.OK) {

                        } else if (action.get() == ButtonType.CANCEL) {

                        }
                    }
                }
            }
        }
    }

    private void lister() {

        tableFacture.getItems().clear();
        List<Facture> liste = factureDAO.listerFacturesAPayer();
        liste.forEach((m) -> {

            data.add(m);
        });
        tableFacture.refresh();
        tableFacture.setItems(data);
    }

}
