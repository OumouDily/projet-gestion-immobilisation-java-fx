/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.EquipementDAO;
import DAO.VenteDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Equipement;
import Models.Vente;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class ListeEquipEnVenteController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private TableView<Equipement> listeVente;
    private TableColumn<Equipement, String> numEquipColumn;
    @FXML
    private TableColumn<Equipement, String> typeEquipColumn;
//    private TableColumn<Equipement, > compteEquipColumn;
    @FXML
    private TableColumn<Equipement, String> nomEquipColumn;
    private TableColumn<Equipement, String> nomFourniColumn;
    @FXML
    private TableColumn<Equipement, String> nomAgentColumn;
    @FXML
    private TableColumn<Equipement, Double> montantEquipColumn;
    @FXML
    private Button btnAjout;
    private TextField txtNum;
    @FXML
    private TextField txtType;
    @FXML
    private TextField txtPrix;
    @FXML
    private TextField txtNom;
    @FXML
    private TextField txtUser;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.equipementDAO = daoFactory.getEquipementDAO();
        this.venteDAO = daoFactory.getVenteDAO();
        lister();
//        numEquipColumn.setCellValueFactory(new PropertyValueFactory<>("cde_equip"));
        typeEquipColumn.setCellValueFactory(new PropertyValueFactory<>("type_immo"));
        montantEquipColumn.setCellValueFactory(new PropertyValueFactory<>("montant"));
        nomEquipColumn.setCellValueFactory(new PropertyValueFactory<>("nom_equip"));
        nomAgentColumn.setCellValueFactory(new PropertyValueFactory<>("nom_agent"));
        
    }

    Message message;
    EquipementDAO equipementDAO;
    VenteDAO venteDAO;
    DAOFactory daoFactory;
    ObservableList<Equipement> data = FXCollections.observableArrayList();

    @FXML
    private void retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/vente.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    private void lister() {
        listeVente.getItems().clear();
        List<Equipement> liste = equipementDAO.listerEquipementsVente();
        liste.forEach((m) -> {

//            String cde_equip = m.getCde_equip();
            String type_immo = m.getType_immo();
            Double montant = m.getMontant();
            String nom_equip = m.getNom_equip();
            String nom_agent = m.getNom_agent();

            data.add(m);
        });
        listeVente.refresh();
        listeVente.setItems(data);
    }

    @FXML
    private void ajouterPanier(ActionEvent event) {
//        String codeVente = txtNum.getText();
        String typeImmo = txtType.getText();
        String nomImmo = txtNom.getText();
        Double prixVente = Double.parseDouble(txtPrix.getText().trim());
        String nomAcheteur = txtUser.getText();

        if (!typeImmo.isEmpty() && !nomImmo.isEmpty() && !nomAcheteur.isEmpty()) {
            Vente vente = new Vente();
            vente.setCodeVente(null);
            vente.setEtat("NON VENDU");
            vente.setTypeImmo(typeImmo);
            vente.setNomImmo(nomImmo);
            vente.setPrixVente(prixVente);
            vente.setNomAcheteur(nomAcheteur);

            if (venteDAO.Ajouter(vente)) {
                message = new Message("INFORMATION", "INFORMATION", "Ajouté au panier", "Ajouté");
//                txtNum.clear();
                txtType.clear();
                txtPrix.clear();
                txtNom.clear();
                txtUser.clear();

            } else {
                message = new Message("ERROR", "Erreur", "INFORMATION", "Echec de l'ajout");
            }

        } else {
//            message = new Message("ERROR", "Erreur", "Inscription", "Au moins un des champs est vide");
        }
    }
    ObservableList<Equipement> ligneSelectionner = null;

    @FXML
    private void selectionnerEquip(MouseEvent event) {
        ligneSelectionner = listeVente.getSelectionModel().getSelectedItems();
        ligneSelectionner.forEach((ls) -> {
//            txtNum.setText(ls.getCde_equip());
            txtType.setText(ls.getType_immo());
            txtPrix.setText(String.valueOf(ls.getMontant()));
            txtNom.setText(ls.getNom_equip());
            txtUser.setText(ls.getNom_agent());
        });
    }
}
