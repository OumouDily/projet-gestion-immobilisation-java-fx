/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DAO.EquipementDAO;
import DAOFactory.DAOFactory;
import Message.Message;
import Models.Equipement;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * FXML Controller class
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class ListeEquipementsController implements Initializable {

    @FXML
    private Hyperlink retour;
    @FXML
    private Hyperlink deconnexion;
    @FXML
    private TableView<Equipement> tableEtat;
    private TableColumn<Equipement, String> cdeImmo;
    @FXML
    private TableColumn<Equipement, String> typeImmo;
    @FXML
    private TableColumn<Equipement, String> etatColumn;
    @FXML
    private TableColumn<Equipement, Date> dateMES;
    @FXML
    private TableColumn<Equipement, String> nomImmo;
    @FXML
    private TableColumn<Equipement, Double> prixAchat;
    @FXML
    private TableColumn<Equipement, String> nomUser;
    @FXML
    private TableColumn<Equipement, String> validationColumn;
    @FXML
    private TextField txtTypeImmo;
    @FXML
    private TextField txtEtatImmo;
    @FXML
    private DatePicker dateImmo;
    @FXML
    private TextField txtNomImmo;
    @FXML
    private TextField txtPrixAchat;
    @FXML
    private TextField txtNomUser;
    @FXML
    public TextField txtIdSession;
    @FXML
    public TextField txtNomSession;
    @FXML
    public TextField txtEmailSession;
    @FXML
    public TextField txtMdpSession;
    @FXML
    public TextField txtNom;
    @FXML
    public TextField txtEmail;
    @FXML
    public TextField txtMdp;
    @FXML
    private Button btnValider;
    @FXML
    private Button btnImprimer;
    @FXML
    private Button btnExporter;
    @FXML
    private Button btnAmortis;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.daoFactory = DAOFactory.getInstance();
        this.equipementDAO = daoFactory.getEquipementDAO();
        lister();
//        cdeImmo.setCellValueFactory(new PropertyValueFactory<>("cde_equip"));
        typeImmo.setCellValueFactory(new PropertyValueFactory<>("type_immo"));
        etatColumn.setCellValueFactory(new PropertyValueFactory<>("etat"));
        dateMES.setCellValueFactory(new PropertyValueFactory<>("date"));
        nomImmo.setCellValueFactory(new PropertyValueFactory<>("nom_equip"));
        prixAchat.setCellValueFactory(new PropertyValueFactory<>("montant"));
        nomUser.setCellValueFactory(new PropertyValueFactory<>("nom_agent"));
        validationColumn.setCellValueFactory(new PropertyValueFactory<>("validation"));
    }
    Message message;
    EquipementDAO equipementDAO;

    DAOFactory daoFactory;
    ObservableList<Equipement> data = FXCollections.observableArrayList();

    @FXML
    private void retourner(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View/Menu.fxml"));
            loader.load();
            MenuController mc = loader.getController();
            mc.txtIdSession.setText(txtIdSession.getText());
            mc.txtEmailSession.setText(txtEmail.getText());
            mc.txtMdpSession.setText(txtMdp.getText());
            mc.txtNomSession.setText(txtNom.getText());
            Parent root = loader.getRoot();
//                            Application.setUserAgentStylesheet(STYLESHEET_MODENA);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
//                            stage.getIcons().add(new Image("/gestionstock/media/logo.png"));
            stage.setTitle("");
            stage.sizeToScene();
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @FXML
    private void deconnecter(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/PageAuthentification.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }

    private void lister() {
        tableEtat.getItems().clear();
        List<Equipement> liste = equipementDAO.listerEquipements();
        liste.forEach((m) -> {

//            String cde_equip = m.getCde_equip();
            String type_immo = m.getType_immo();
            String etat = m.getEtat();
            Date date = m.getDate();
            String nom_equip = m.getNom_equip();
            Double montant = m.getMontant();
            String nom_agent = m.getNom_agent();
            data.add(m);
        });
        tableEtat.refresh();
        tableEtat.setItems(data);
    }

    @FXML
    private void valider(ActionEvent event) {
        String nom = txtNomImmo.getText().trim();

        String type_immo = txtTypeImmo.getText();
        String etat = txtEtatImmo.getText();
//        LocalDate ld = dateMES.getValue();
//        Date sqlDate = Date.valueOf(ld);

        String nom_equip = txtNomImmo.getText();
        Double montant = Double.parseDouble(txtPrixAchat.getText());
        String nom_agent = txtNomUser.getText();
        if (!type_immo.isEmpty() && !etat.isEmpty() && !nom_equip.isEmpty() && !nom_agent.isEmpty()) {
            Equipement equipement = equipementDAO.rechercherParNom(nom);

            if (equipement != null) {

                equipement.setType_immo(equipement.getType_immo());

                equipement.setEtat(equipement.getEtat());

                equipement.setDate(equipement.getDate());
                equipement.setNom_equip(equipement.getNom_equip());
                equipement.setMontant(equipement.getMontant());
                equipement.setNom_agent(equipement.getNom_agent());
                equipement.setValidation("Validé");

                if (equipementDAO.modifierEquipement(equipement)) {
                    lister();
                    message = new Message("INFORMATION", "INFORMATION", "Equipement", "Validé");

                } else {
                    message = new Message("ERROR", "Erreur", "Equipement", "Echec de la validation");
                }
            } else {
                System.out.println("erreur");
            }

        } else {
//            message = new Message("ERROR", "Erreur", "Inscription", "Au moins un des champs est vide");
        }
    }
    ObservableList<Equipement> ligneSelectionner = null;

    @FXML
    private void selectionner(MouseEvent event) {
        ligneSelectionner = tableEtat.getSelectionModel().getSelectedItems();
        ligneSelectionner.forEach((ls) -> {
//            txtTypeImmo.setText(String.valueOf(ls.getId()));
            txtTypeImmo.setText(ls.getType_immo());
            txtEtatImmo.setText(ls.getEtat());
//            dateImmo.setText(String.valueOf(ls.getType_immo()));
            dateImmo.setValue(LocalDate.parse(String.valueOf(ls.getDate())));
            txtNomImmo.setText(ls.getNom_equip());
            txtPrixAchat.setText(String.valueOf(ls.getMontant()));
            txtNomUser.setText(ls.getNom_agent());
        });
    }

    @FXML
    private void imprimer(ActionEvent event) throws JRException, IOException, SQLException {
        try {
            String path = "C:/ProgramData/ProjetImmo/listeEquipements.jrxml";
            File file = new File(path);
            InputStream is = new FileInputStream(file);
            JasperDesign jasperDesign = JRXmlLoader.load(is);
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            Map parameter = new HashMap();
            parameter.put("titre", "Liste des equipements");
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter, daoFactory.getConnection());
            JasperExportManager.exportReportToPdfFile(jasperPrint, "C:/ProgramData/ProjetImmo/listeEquipements.pdf");
            Desktop.getDesktop().open(new File("C:/ProgramData/ProjetImmo/listeEquipements.pdf"));
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        }
    }
    Integer index;

    @FXML
    private void exporterExcel(ActionEvent event) throws FileNotFoundException, IOException {
        try {
            XSSFWorkbook wb = new XSSFWorkbook();
            XSSFSheet sheet = wb.createSheet("LISTE_DES_EQUIPEMENTS");
            XSSFRow header = sheet.createRow(0);
            header.createCell(0).setCellValue("Type");
            header.createCell(1).setCellValue("Etat");
            header.createCell(2).setCellValue("Date");
            header.createCell(3).setCellValue("Nom de l'equipement");
            header.createCell(4).setCellValue("Prix d'achat");
            header.createCell(5).setCellValue("Nom de l'utilisateur");
            header.createCell(6).setCellValue("Validation");
//            header.createCell(4).setCellValue("Prix d'achat");
            sheet.setColumnWidth(0, 256 * 50);
            sheet.setColumnWidth(1, 256 * 15);
            sheet.setColumnWidth(2, 256 * 13);
            sheet.setColumnWidth(3, 256 * 22);
            sheet.setColumnWidth(4, 256 * 10);
            sheet.setColumnWidth(5, 256 * 20);
            sheet.setColumnWidth(6, 256 * 10);
//            sheet.autoSizeColumn(1);
//            sheet.autoSizeColumn(3);
//            sheet.autoSizeColumn(4);
            sheet.setZoom(150);
            List<Equipement> liste = equipementDAO.listerEquipements();
            index = 1;
            liste.forEach((values) -> {
                XSSFRow row = sheet.createRow(index);
                row.createCell(0).setCellValue(values.getType_immo());
                row.createCell(1).setCellValue(values.getEtat());
                String dateFormater = sdf.format(values.getDate());
                row.createCell(2).setCellValue(dateFormater);
                row.createCell(3).setCellValue(values.getNom_equip());
                row.createCell(4).setCellValue(values.getMontant());
                row.createCell(5).setCellValue(values.getNom_agent());
                row.createCell(6).setCellValue(values.getValidation());
                index++;
            });
            try (FileOutputStream fileOut = new FileOutputStream("C:/ProgramData/ProjetImmo/ListeEquipements.xlsx")) {
                wb.write(fileOut);
                msg = new Message("INFORMATION", "Information", "Exportation", "Effectuée avec succès !");
                Desktop.getDesktop().open(new File("C:/ProgramData/ProjetImmo/ListeEquipements.xlsx"));
            }
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    Message msg;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @FXML
    private void Amortir(ActionEvent event) {
         ((Node) (event.getSource())).getScene().getWindow().hide();
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("/View/listeEquipementsAmortis.fxml"));
        try {
            Loader.load();
        } catch (IOException ex) {
        }
        Parent root = Loader.getRoot();
        Stage stage = new Stage();

//        stage.setResizable(false);
        stage.setTitle("");
//       stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();
    }


}
