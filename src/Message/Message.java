/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Message;

import javafx.scene.control.Alert;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class Message {
    public Message(String type, String titre, String contexte, String message) {
        Alert alert = new Alert(Alert.AlertType.NONE);
        switch (type) {
            case "ERROR":
                alert.setAlertType(Alert.AlertType.ERROR);
                break;
            case "INFORMATION":
                alert.setAlertType(Alert.AlertType.INFORMATION);
                break;
            case "WARNING":
                alert.setAlertType(Alert.AlertType.WARNING);
                break;
        }
        alert.setTitle(titre);
        alert.setHeaderText(contexte);
        alert.setContentText(message);
        alert.showAndWait();
    }
}
