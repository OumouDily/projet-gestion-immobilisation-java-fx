/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOFactory;

import DAO.AgenceDAO;
import DAO.BlacklistDAO;
import DAO.DepartementDAO;
import DAO.EmployeDAO;
import DAO.EquipementDAO;
import DAO.FactureDAO;
import DAO.FournisseurDAO;
import DAO.MembreDAO;
import DAO.PaiementDAO;
import DAO.ServiceDAO;
import DAO.VenteDAO;
import DAOImpl.AgenceDAOImpl;
import DAOImpl.BlacklistDAOImpl;
import DAOImpl.DepartementDAOImpl;
import DAOImpl.EmployeDAOImpl;
import DAOImpl.EquipementDAOImpl;
import DAOImpl.FactureDAOImpl;
import DAOImpl.FournisseurDAOImpl;
import DAOImpl.MembreDAOImpl;
import DAOImpl.PaiementDAOImpl;
import DAOImpl.ServiceDAOImpl;
import DAOImpl.VenteDAOImpl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author PIC-TECHNOLOGY SARL
 */
public class DAOFactory {
     private final String url;
    private final String username;
    private final String password;

    public DAOFactory(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public static DAOFactory getInstance() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
        }
        DAOFactory instance = new DAOFactory("jdbc:mysql://127.0.0.1:3306/imm", "root", "");
        return instance;
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }

    public MembreDAO getMembreDAO() {
        return new MembreDAOImpl(this);
    }
    
    public DepartementDAO getDepartementDAO() {
        return new DepartementDAOImpl(this);
    }
    
    public ServiceDAO getServiceDAO() {
        return new ServiceDAOImpl(this);
    }
    
    public FournisseurDAO getFournisseurDAO() {
        return new FournisseurDAOImpl(this);
    }
    
    public FactureDAO getFactureDAO() {
        return new FactureDAOImpl(this);
    }
    
     public BlacklistDAO getBlacklistDAO() {
        return new BlacklistDAOImpl(this);
    }
     
     public EmployeDAO getEmployeDAO() {
        return new EmployeDAOImpl(this);
    }
   
    public AgenceDAO getAgenceDAO() {
        return new AgenceDAOImpl(this);
    }
    
    public EquipementDAO getEquipementDAO() {
        return new EquipementDAOImpl(this);
    }
    
     public PaiementDAO getPaiementDAO() {
        return new PaiementDAOImpl(this);
    }
     
     public VenteDAO getVenteDAO() {
        return new VenteDAOImpl(this);
    }
     
     
}
